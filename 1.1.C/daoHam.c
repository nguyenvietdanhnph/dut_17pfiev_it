#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#define MAX_LENGTH 200

typedef int( * cmpfn)(const void * ,
  const void * );

char s[MAX_LENGTH];
int thu = 0;
int stt = 0;

struct sohang_Type {
  int heso;
  int bac;
}
sohang[MAX_LENGTH];
struct daoham_Type {
  int heso;
  int bac;
}
daoham[MAX_LENGTH];
/*-----------------------------*/
int cmp(struct sohang_Type * a, struct sohang_Type * b) {
  return (a -> bac) - (b -> bac);
}
void Sort() {
  qsort(sohang, thu, sizeof(struct sohang_Type), (cmpfn) cmp);
}
void add(int stt, int i) {
  daoham[stt].heso += sohang[i].heso;
  daoham[stt].bac = sohang[i].bac;
}
void daoHam() {
  for (int i = 0; i < thu; i++) {
    add(stt, i);
    if ((sohang[i].bac != sohang[i + 1].bac) || (i >= thu - 1)) {
      stt++;
    }
  }
  for (int i = 0; i < stt; i++) {
    if (daoham[i].bac == 0) daoham[i].heso = 0;
    else {
      daoham[i].heso *= (daoham[i].bac) --;
    }
  }
}
int isDigit(char c) {
  return ((c >= '0') && (c <= '9'));
}
void Mystrcat(char * s, char c) {
  int temp = strlen(s);
  s[temp] = c;
  s[temp + 1] = '\0';
}
void getData(char * s) {
  FILE * f = fopen("input.txt", "r");
  char c;
  int i = 0;
  while ((c = fgetc(f)) != EOF) {
    if ((c == '+') || (c == '-') || (c == 'x') || (c == 'X') || (c == '^') || ((c >= '0') && (c <= '9'))) {
      if (c == 'X') s[i++] = 'x';
      else s[i++] = c;
    }
  }
  s[i] = '\0';
  fclose(f);
  i = 0;
  int dau = 1;
  char integer[MAX_LENGTH];
  char mu_integer[MAX_LENGTH];
  memset(mu_integer, '\0', MAX_LENGTH);
  while (i <= strlen(s) - 1) {
    dau = 1;
    while (!isDigit(s[i]) && s[i] != 'x') //tim dau -
    {
      if (s[i] == '-') dau = -1;
      i++;
    }
    while (isDigit(s[i])) //lay heso
    {
      Mystrcat(integer, s[i]);
      i++;
    }
    if (integer[0] == '\0') sohang[thu].heso = 1; //truong hop khong ghi he so
    else sohang[thu].heso = dau * atoi(integer); //truong hop co ghi he so
    memset(integer, '\0', MAX_LENGTH); //dua bien integer ve 0
    if (s[i] == 'x') //bat dau lay so mu
    {
      i++;
      if (s[i] == '^') //truong hop co nhap ^
      {
        i++;
        dau = 1;
        if (s[i] == '-') { //mu la so am
          dau = -1;
          i++;
        } else if (s[i] == '+') i++; //mu ghi du dau + thi bo qua
        while (isDigit(s[i])) {
          Mystrcat(mu_integer, s[i]);
          i++;
        }
        sohang[thu].bac = dau * atoi(mu_integer);
      } else {
        sohang[thu].bac = 1;
      }
    } else sohang[thu].bac = 0;
    memset(mu_integer, '\0', MAX_LENGTH);
    thu++;
  }
}
void outData() {
    FILE * f = fopen("output.txt", "w");
    int i;
    for (i = 0; i < stt - 1; i++) {
      if (daoham[i].heso != 0) {
        fprintf(f, "%d", daoham[i].heso);
        if (daoham[i].bac != 0) {
          fprintf(f, "*x");
          if (daoham[i].bac != 1) {
            if (daoham[i].bac >= 2) fprintf(f, "^%d", daoham[i].bac);
            else fprintf(f, "^(%d)", daoham[i].bac);
          }
        }
      }
      if (daoham[i + 1].heso > 0) fprintf(f, "+");
    }
    if (daoham[i].heso != 0) {
      fprintf(f, "%d", daoham[i].heso);
      if (daoham[i].bac != 0) {
        fprintf(f, "*x");
        if (daoham[i].bac != 1) {
          if (daoham[i].bac >= 2) fprintf(f, "^%d", daoham[i].bac);
          else fprintf(f, "^(%d)", daoham[i].bac);
        }
      }
    }
    fclose(f);
  }
  /*===================================================================*/
int main() {
  getData(s);
  //printf("%s\n",s);
  Sort();
  daoHam();
  /*for (int i=0;i<thu;i++)
  {
      printf(" + %d x^ %d",sohang[i].heso,sohang[i].bac);
  }
  printf("\n");
  for (int i=0;i<stt;i++)
  {
      printf(" + %d x^ %d",daoham[i].heso,daoham[i].bac);
  }*/
  outData();
  return 0;
}
