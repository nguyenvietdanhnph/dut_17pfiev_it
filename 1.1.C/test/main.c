#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_LENGTH 200

char s[MAX_LENGTH];
int thu=0;
int id_heso=0;
int temp;
int id_mu=1;
//float max_bac=0;
struct huuty_Type {
    float tu;
    float mau;
};

struct sohang_Type {
    struct huuty_Type heso;
    int bac;
} sohang[100];

int isDigit(char c) {
    return ((c>='0') && (c<='9'));
}
int isDigitOrMinus(char c) {
    return ((c>='0') && (c<='9')) || (c=='-');
}
void huuTy(FILE *f) {
    if (!id_mu) sohang[thu++].bac=1;   //truong hop ko nhap so mu
    fseek(f,temp,SEEK_SET);
    char c;
    fscanf(f,"%f",&sohang[thu].heso.tu);
    while ((c=fgetc(f)) != '/');
    fscanf(f,"%f",&sohang[thu].heso.mau);
    id_mu=0;
    id_heso=1;
}
void thuc(FILE *f) {
    if (!id_mu) sohang[thu++].bac=1;     //truong hop ko nhap so mu
    temp=ftell(f)-1;
    fseek(f,temp,SEEK_SET);
    fscanf(f,"%f",&sohang[thu].heso.tu);
    sohang[thu].heso.mau=1;
    id_mu=0;
    id_heso=1;
    /*char c;
    while ((c=fgetc(f)) == ' ');
    if ((c!='*') && (c!='x')) {
        sohang[thu].bac=0;
    }*/
}
void mu_nguyen(FILE *f) {
    fscanf(f,"%d",&sohang[thu].bac);
    thu++;
    id_mu=1;
    id_heso=0;
}
void getData() {
    FILE *f=fopen("input.txt","r");
    char c;
    while ((c=fgetc(f)) != EOF) {
            if (isDigitOrMinus(c)) thuc(f);
            if (c=='/') huuTy(f);
            if (c=='^') mu_nguyen(f);
            if ((c=='x') || c=='X') && (!id_heso)) {
                sohang[thu].heso.mau=sohang[thu].heso.tu=1;
                id_heso=1;
                id_mu=0;
            }
    }
    fclose(f);
}
int main() {
    getData();
    for (int i=0;i<=thu;i++)
    {
        printf(" + %0.2f/%0.2f x^ %d",sohang[i].heso.tu,sohang[i].heso.mau,sohang[i].bac);
    }
}


