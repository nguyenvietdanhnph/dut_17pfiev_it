#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_LENGTH 200

char s[MAX_LENGTH];
int thu=0;
float max_bac=0;

struct sohang_Type {
    float heso;
    int bac;
} sohang[100];
/*-----------------------------*/
int isDigit(char c) {
    return ((c>='0') && (c<='9'));
}
void Mystrcat(char *s,char c) {
    int temp=strlen(s);
    s[temp]=c;
    s[temp+1]='\0';
}
void getData(char *s) {
    FILE *f=fopen("input.txt","r");
    char c;
    int i=0;
    while ((c=fgetc(f)) != EOF) {
        if ((c=='+') || (c=='-') || (c=='x') || (c=='X') || (c=='^') || ((c>='0') && (c<='9')))
        {
            if (c=='X') s[i++]='x';
            else s[i++]=c;
        }
    }
    s[i]='\0';
    fclose(f);
    i=0;
    int dau=1;
    char integer[MAX_LENGTH];
    char mu_integer[MAX_LENGTH];
    memset(mu_integer,'\0',MAX_LENGTH);
    while (i<=strlen(s)-1) {
        dau=1;
        while (!isDigit(s[i]) && s[i]!='x')
        {
            if (s[i]=='-') dau=-1;
            i++;
        }
        while (isDigit(s[i]))
        {
            Mystrcat(integer,s[i]);
            i++;
        }
        if (integer[0]=='\0') sohang[thu].heso=1;
            else sohang[thu].heso=dau*atoi(integer);
        memset(integer,'\0',MAX_LENGTH);
        if  (s[i]=='x')
        {
            i++;
            if (s[i]=='^')
            {
                i++;
                dau=1;
                if (s[i]=='-') {
                    dau=-1;
                    i++;
                }
            else if (s[i]=='+') i++;
            while (isDigit(s[i]))
            {
                Mystrcat(mu_integer,s[i]);
                i++;
            }
        sohang[thu].bac=dau*atoi(mu_integer);
        }
        else {
                sohang[thu].bac=1;
             }
    }
    else sohang[thu].bac=0;
    memset(mu_integer,'\0',MAX_LENGTH);
    thu++;
    }
}

int main() {
    getData(s);
    printf("%s\n",s);
    for (int i=0;i<thu;i++)
    {
        printf(" + %d x^ %d",sohang[i].heso,sohang[i].bac);
    }
    return 0;
}
/*================================================*/
