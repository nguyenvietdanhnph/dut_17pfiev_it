#include <iostream>
#include <fstream>
#include <codecvt>
#include <regex>
#include <string>
#include <algorithm>

class node
{
public:
    int count;
    std::wstring word;
    node *left;
    node *right;
    node();
    node(std::wstring s_word);
    ~node(){};
    node *insert(node *n, std::wstring s_word);
    void inTraverse(node *n);
};

node::node() : count(0), word(L""), left(NULL), right(NULL){};
node::node(std::wstring s_word) : count(1), word(s_word), left(NULL), right(NULL){};

node *node::insert(node *n, std::wstring s_word)
{
    if (!n)
    {
        return new node(s_word);
    }
    if (s_word == n->word)
    {
        n->count++;
    }
    else if (s_word > n->word)
    {
        n->right = insert(n->right, s_word);
    }
    else
    {
        n->left = insert(n->left, s_word);
    }
    return n;
}
void node::inTraverse(node *n)
{
    if (!n)
        return;
    inTraverse(n->left);
    std::wcout << n->word << L": " << n->count << std::endl;
    inTraverse(n->right);
}

int main(int argc, char *argv[])
{
    std::locale::global(std::locale("en_US.utf8")); //Định dạng UTF-8 để viết Unicode
    std::wcout.imbue(std::locale());
    std::wcout << L"Chương trình thống kê số từ của văn bản (Mã hóa UTF-8)" << std::endl;
    std::wifstream file;
    if (argc == 1)
    {
        std::wcout << L"Error: Chưa nhập đường dẫn đến file" << std::endl;
    }
    else
    {
        for (int i = 1; i < argc; i++)
        {
            std::wcout << L"------------------------------------------------------" << std::endl;
            std::wcout << L"Đang thống kê file '" << argv[i] << L"'" << std::endl;
            file.open(argv[i]);

            node *root = NULL;

            if (file.is_open())
            {
                file.imbue(std::locale(file.getloc(), new std::codecvt_utf8<wchar_t>)); //Quy định file mã hóa dạng UTF-8
                std::wstring line;
                std::wregex word_pattern(L"\\b[-?(\\w+)?]+\\b");
                while (getline(file, line))
                {
                    std::wsregex_iterator currentMatch(line.begin(), line.end(), word_pattern);
                    std::wsregex_iterator lastMatch;
                    for (; currentMatch != lastMatch; currentMatch++)
                    {
                        std::wsmatch match = *currentMatch;
                        std::wstring a = match.str();
                        std::transform(a.begin(), a.end(), a.begin(), ::tolower);
                        root = root->insert(root, a);
                    }
                }
                file.close();
            }
            std::wcout << L"<Từ>: <Số lần>" << std::endl;
            root->inTraverse(root);
        }
    }
    return 0;
}