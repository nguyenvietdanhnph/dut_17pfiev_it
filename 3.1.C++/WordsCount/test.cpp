#include <iostream>
#include <fstream>
#include <regex>
#include <codecvt>
#include <sstream>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

bool test_unicode()
{
    // std::locale old;
    // std::locale::global(std::locale("en_US.UTF-8"));

    std::wregex pattern(L"[[:alpha:]]+");
    bool result = std::regex_match(std::wstring(L"abcdéfg"), pattern);

    // std::locale::global(old);

    return result;
}

int main() {
       std::locale::global(std::locale("en_US.utf8"));
// std::wcout.imbue(std::locale());

    std::wifstream vao("file.txt");
    vao.imbue(std::locale(vao.getloc(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>));
    std::wstring chuoi;
    std::getline(vao, chuoi);
    std::wcout<<chuoi;
    std::wcout<<"Âbc";
    std::wcout<<test_unicode();
// std::wstring s = L"šđčćžŠĐČĆŽ";
// std::wcout << s;
    vao.close();
    
    std::wifstream vao2("file2.txt");
    vao2.imbue(std::locale(vao.getloc(),new std::codecvt_utf8<wchar_t>));
    std::getline(vao2, chuoi);
    std::wcout<<chuoi;
    return 0;
}