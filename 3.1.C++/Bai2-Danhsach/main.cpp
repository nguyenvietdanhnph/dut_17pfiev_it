#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;
const int SBD_WIDTH = 10;
const int TEN_WIDTH = 30;
const int DIEM_WIDTH = 10;
const int KHOITHI_WIDTH = 9;
const int NGANHTHI_WIDTH = 50;

struct nganhthi_type {
    string tennganh;
    int hs1, hs2, hs3;
    float diemchuan;
};
struct thisinh_type {
    long sbd;
    string hoten;
    float toan, ly, hoa;
    float diemtong;
    string khoithi;
    string nganhthi;
};

int soluongthisinh=0;
int soluongnganh=0;
thisinh_type thisinh[10];
nganhthi_type nganhthi[10];

int binarySearch(thisinh_type arr[], int l, int r, int x);
bool compare2NganhTHi(thisinh_type a, thisinh_type b);
bool compare2Sbd(thisinh_type a, thisinh_type b);
bool compare2Ten(thisinh_type a, thisinh_type b);
int findNganhIndex(string tennganh);
bool isNumber(string x);
void nhapDuLieu(string dirData, string dirHeso);

int main() {
    nhapDuLieu("data.csv", "heso.csv");
    //xuat du lieu
    cout<<"Du lieu:"<<endl;
    cout<<left;
    cout<<setw(SBD_WIDTH)<<"Sbd"<<setw(TEN_WIDTH)<<"Ho va ten"<<setw(DIEM_WIDTH)<<"Toan"<<setw(DIEM_WIDTH)<<"Ly"<<setw(DIEM_WIDTH)<<"Hoa"<<setw(DIEM_WIDTH)<<"Diem tong"<<setw(KHOITHI_WIDTH)<<"Khoi thi"<<setw(NGANHTHI_WIDTH)<<"Nganh thi"<<endl;
    for (int i=1; i<=soluongthisinh; i++) {
        cout<<setw(SBD_WIDTH)<<thisinh[i].sbd<<setw(TEN_WIDTH)<<thisinh[i].hoten<<setw(DIEM_WIDTH)<<thisinh[i].toan<<setw(DIEM_WIDTH)<<thisinh[i].ly<<setw(DIEM_WIDTH)<<thisinh[i].hoa<<setw(DIEM_WIDTH)<<thisinh[i].diemtong<<setw(KHOITHI_WIDTH)<<thisinh[i].khoithi<<setw(NGANHTHI_WIDTH)<<thisinh[i].nganhthi<<endl;
    }
    cout<<endl;

    //xuat theo ten Alphabet
    cout<<"Sap xep theo ten Alphabet:"<<endl;
    sort(thisinh, thisinh + soluongthisinh, compare2Ten);
    cout<<setw(SBD_WIDTH)<<"Sbd"<<setw(TEN_WIDTH)<<"Ho va ten"<<setw(DIEM_WIDTH)<<"Toan"<<setw(DIEM_WIDTH)<<"Ly"<<setw(DIEM_WIDTH)<<"Hoa"<<setw(DIEM_WIDTH)<<"Diem tong"<<setw(KHOITHI_WIDTH)<<"Khoi thi"<<setw(NGANHTHI_WIDTH)<<"Nganh thi"<<endl;
    for (int i=1; i<=soluongthisinh; i++) {
        cout<<setw(SBD_WIDTH)<<thisinh[i].sbd<<setw(TEN_WIDTH)<<thisinh[i].hoten<<setw(DIEM_WIDTH)<<thisinh[i].toan<<setw(DIEM_WIDTH)<<thisinh[i].ly<<setw(DIEM_WIDTH)<<thisinh[i].hoa<<setw(DIEM_WIDTH)<<thisinh[i].diemtong<<setw(KHOITHI_WIDTH)<<thisinh[i].khoithi<<setw(NGANHTHI_WIDTH)<<thisinh[i].nganhthi<<endl;
    }
    cout<<endl;

    //sap xep theo sbd
    sort(thisinh, thisinh+soluongthisinh, compare2Sbd);
    cout<<"Sap xep theo Sbd:"<<endl;
    cout<<setw(SBD_WIDTH)<<"Sbd"<<setw(TEN_WIDTH)<<"Ho va ten"<<setw(DIEM_WIDTH)<<"Toan"<<setw(DIEM_WIDTH)<<"Ly"<<setw(DIEM_WIDTH)<<"Hoa"<<setw(DIEM_WIDTH)<<"Diem tong"<<setw(KHOITHI_WIDTH)<<"Khoi thi"<<setw(NGANHTHI_WIDTH)<<"Nganh thi"<<endl;
    for (int i=1; i<=soluongthisinh; i++) {
        cout<<setw(SBD_WIDTH)<<thisinh[i].sbd<<setw(TEN_WIDTH)<<thisinh[i].hoten<<setw(DIEM_WIDTH)<<thisinh[i].toan<<setw(DIEM_WIDTH)<<thisinh[i].ly<<setw(DIEM_WIDTH)<<thisinh[i].hoa<<setw(DIEM_WIDTH)<<thisinh[i].diemtong<<setw(KHOITHI_WIDTH)<<thisinh[i].khoithi<<setw(NGANHTHI_WIDTH)<<thisinh[i].nganhthi<<endl;
    }
    cout<<endl;

    //sap xep theo diem chuan
    sort(thisinh,thisinh+soluongthisinh, compare2NganhTHi);
    cout<<"Thi sinh trung tuyen:"<<endl;
    cout<<setw(SBD_WIDTH)<<"Sbd"<<setw(TEN_WIDTH)<<"Ho va ten"<<setw(DIEM_WIDTH)<<"Toan"<<setw(DIEM_WIDTH)<<"Ly"<<setw(DIEM_WIDTH)<<"Hoa"<<setw(DIEM_WIDTH)<<"Diem tong"<<setw(KHOITHI_WIDTH)<<"Khoi thi"<<setw(NGANHTHI_WIDTH)<<"Nganh thi"<<endl;
    for (int i=1; i<=soluongthisinh; i++) {
        if (thisinh[i].diemtong >= nganhthi[findNganhIndex(thisinh[i].nganhthi)].diemchuan) {
            cout<<setw(SBD_WIDTH)<<thisinh[i].sbd<<setw(TEN_WIDTH)<<thisinh[i].hoten<<setw(DIEM_WIDTH)<<thisinh[i].toan<<setw(DIEM_WIDTH)<<thisinh[i].ly<<setw(DIEM_WIDTH)<<thisinh[i].hoa<<setw(DIEM_WIDTH)<<thisinh[i].diemtong<<setw(KHOITHI_WIDTH)<<thisinh[i].khoithi<<setw(NGANHTHI_WIDTH)<<thisinh[i].nganhthi<<endl;
        }
    }
    cout<<endl;

    cout<<"Nhap Sbd hoac Ten cua thi sinh muon xem: ";
    long sobaodanh;
    string ten;
    getline(cin, ten);
    if (isNumber(ten)) {
        sobaodanh=stol(ten);
        cout<<setw(SBD_WIDTH)<<"Sbd"<<setw(TEN_WIDTH)<<"Ho va ten"<<setw(DIEM_WIDTH)<<"Toan"<<setw(DIEM_WIDTH)<<"Ly"<<setw(DIEM_WIDTH)<<"Hoa"<<setw(DIEM_WIDTH)<<"Diem tong"<<setw(KHOITHI_WIDTH)<<"Khoi thi"<<setw(NGANHTHI_WIDTH)<<"Nganh thi"<<endl;
        int i=findNganhIndex(thisinh[binarySearch(thisinh, 1, soluongthisinh, sobaodanh)].nganhthi);
        cout<<setw(SBD_WIDTH)<<thisinh[i].sbd<<setw(TEN_WIDTH)<<thisinh[i].hoten<<setw(DIEM_WIDTH)<<thisinh[i].toan<<setw(DIEM_WIDTH)<<thisinh[i].ly<<setw(DIEM_WIDTH)<<thisinh[i].hoa<<setw(DIEM_WIDTH)<<thisinh[i].diemtong<<setw(KHOITHI_WIDTH)<<thisinh[i].khoithi<<setw(NGANHTHI_WIDTH)<<thisinh[i].nganhthi<<endl;
    }
    else {
        cout<<setw(SBD_WIDTH)<<"Sbd"<<setw(TEN_WIDTH)<<"Ho va ten"<<setw(DIEM_WIDTH)<<"Toan"<<setw(DIEM_WIDTH)<<"Ly"<<setw(DIEM_WIDTH)<<"Hoa"<<setw(DIEM_WIDTH)<<"Diem tong"<<setw(KHOITHI_WIDTH)<<"Khoi thi"<<setw(NGANHTHI_WIDTH)<<"Nganh thi"<<endl;
        for (int i=1; i<=soluongthisinh; i++) {
            if (thisinh[i].hoten.find(ten) != string::npos) {
                cout<<setw(SBD_WIDTH)<<thisinh[i].sbd<<setw(TEN_WIDTH)<<thisinh[i].hoten<<setw(DIEM_WIDTH)<<thisinh[i].toan<<setw(DIEM_WIDTH)<<thisinh[i].ly<<setw(DIEM_WIDTH)<<thisinh[i].hoa<<setw(DIEM_WIDTH)<<thisinh[i].diemtong<<setw(KHOITHI_WIDTH)<<thisinh[i].khoithi<<setw(NGANHTHI_WIDTH)<<thisinh[i].nganhthi<<endl;
            }
        }
    }    
}

/*-----------------------------------------------------------*/
int findNganhIndex(string tennganh) {
    for (int i=1; i<=soluongnganh; i++) {
        if (nganhthi[i].tennganh == tennganh) return i;
    }
    return 0; // khong tim thay ten nganh
}
void nhapDuLieu(string dirData, string dirHeso) {
    ifstream fin;
    fin.open(dirData);
    string line, temp; 
    getline(fin, line);  // bo dong dau
    while (getline(fin, line)) {
        soluongthisinh++;
        stringstream s(line); // chuyen line sang dang stringstream
        getline(s, temp,',');  //sbd
        thisinh[soluongthisinh].sbd=stol(temp);
        getline(s, thisinh[soluongthisinh].hoten, ',');  //hoten
        getline(s, temp, ',');  //toan
        thisinh[soluongthisinh].toan=stof(temp);
        getline(s, temp, ',');  //ly
        thisinh[soluongthisinh].ly=stof(temp);
        getline(s, temp, ','); //hoa
        thisinh[soluongthisinh].hoa=stof(temp);
        getline(s, temp, ','); //diemtong
        getline(s, thisinh[soluongthisinh].khoithi, ','); //khoithi
        getline(s, thisinh[soluongthisinh].nganhthi, ','); //nganhthi
        thisinh[soluongthisinh].nganhthi.pop_back();
    }
    fin.close();
    fin.open(dirHeso);
    while (getline(fin, line)) {
        soluongnganh++;
        stringstream s(line);
        getline(s, nganhthi[soluongnganh].tennganh, ',');
        getline(s,temp, ',');  //  he so 1
        nganhthi[soluongnganh].hs1=stoi(temp);
        getline(s, temp, ','); // he so 2
        nganhthi[soluongnganh].hs2=stoi(temp);
        getline(s, temp, ','); // he so 3
        nganhthi[soluongnganh].hs3=stoi(temp);
        getline(s, temp, ','); // diem chuan
        nganhthi[soluongnganh].diemchuan = stof(temp);
    }
    fin.close();
    //tinh diem tong
        for (int i=1; i<=soluongthisinh; i++) {
            int nganhindex = findNganhIndex(thisinh[i].nganhthi);
            thisinh[i].diemtong=thisinh[i].toan*nganhthi[nganhindex].hs1 + thisinh[i].ly*nganhthi[nganhindex].hs2 + thisinh[i].hoa*nganhthi[nganhindex].hs3;
        }
}
bool compare2Ten(thisinh_type a, thisinh_type b) {
    return a.hoten < b.hoten;
}
bool compare2NganhTHi(thisinh_type a, thisinh_type b) {
    //sap xep nganh thi theo alphabet
    if (a.nganhthi != b.nganhthi) return a.nganhthi < b.nganhthi;
    //sap xep theo diem tong giam dan
    if (a.diemtong != b.diemtong) return a.diemtong > b.diemtong;
    //sap xep theo diem toan giam dan
    return a.toan > b.toan;
}
bool compare2Sbd(thisinh_type a, thisinh_type b) {
    //sap xep sbd alphbet
    return a.sbd < b.sbd;
}
int binarySearch(thisinh_type arr[], int l, int r, int x) { 
    while (l <= r) { 
        int m = l + (r - l) / 2; 
        // Check if x is present at mid 
        if (arr[m].sbd == x) 
            return m; 
        // If x greater, ignore left half 
        if (arr[m].sbd < x) 
            l = m + 1; 
        // If x is smaller, ignore right half 
        else
            r = m - 1; 
    } 
    // if we reach here, then element was 
    // not present 
    return -1; 
} 
bool isNumber(string x) {
    char* p;
    return strtol(x.c_str(), &p, 10) != 0L;
}