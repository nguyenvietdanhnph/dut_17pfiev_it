import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import {
  CssBaseline,
  ThemeProvider,
  unstable_createMuiStrictModeTheme as createTheme,
} from '@material-ui/core'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store, persistor } from 'app/store'
import { PersistGate } from 'redux-persist/integration/react'
import CircularProgress from '@material-ui/core/CircularProgress'

const theme = createTheme({
  typography: {
    fontFamily: "'Montserrat', serif",
    fontSize: 14,
    color: '#625f6e',
  },
  palette: {
    primary: {
      main: '#3f51b5',
    },
    secondary: {
      main: '#f50057',
    },
    background: {
      default: '#F8F8F8',
    },
  },
})

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={<CircularProgress />} persistor={persistor}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <React.StrictMode>
            <CssBaseline />
            <App />
          </React.StrictMode>
        </BrowserRouter>
      </ThemeProvider>
    </PersistGate>
  </Provider>,
  document.getElementById('root')
)
