import { createSlice } from '@reduxjs/toolkit'
import { get } from 'api/axiosClient'
import { createArrayInitialState, createArrayReducer } from 'utils/utilRedux'

const exploreSlice = createSlice({
  name: 'explore',
  initialState: createArrayInitialState(),
  reducers: {
    ...createArrayReducer('exploreGetList'),
  },
})

export const exploreGetList = (query) => async (dispatch, getState) => {
  dispatch(exploreGetListLoading())
  const response = await get('/jobs', { params: query })
  dispatch(exploreGetListSuccess(response))
}

export const {
  exploreGetListLoading,
  exploreGetListSuccess,
  exploreGetListFail,
} = exploreSlice.actions

const exploreReducer = exploreSlice.reducer

export default exploreReducer
