import { createSlice } from '@reduxjs/toolkit'
import { get } from 'api/axiosClient'
import {
  createArrayInitialState,
  createArrayNoPaginateReducer,
} from 'utils/utilRedux'

const favoriteSlice = createSlice({
  name: 'favorite',
  initialState: createArrayInitialState(),
  reducers: {
    ...createArrayNoPaginateReducer('favoriteGetList'),
  },
})

export const favoriteGetList = (query) => async (dispatch, getState) => {
  dispatch(favoriteGetListLoading())
  const response = await get('/jobs/favorites', { params: query })
  dispatch(favoriteGetListSuccess(response))
}

export const {
  favoriteGetListLoading,
  favoriteGetListSuccess,
  favoriteGetListFail,
} = favoriteSlice.actions

const favoriteReducer = favoriteSlice.reducer

export default favoriteReducer
