import { createSlice } from '@reduxjs/toolkit'
import { get } from 'api/axiosClient'
import {
  createArrayInitialState,
  createArrayNoPaginateReducer,
} from 'utils/utilRedux'

const recentSlice = createSlice({
  name: 'recent',
  initialState: createArrayInitialState(),
  reducers: {
    ...createArrayNoPaginateReducer('recentGetList'),
  },
})

export const recentGetList = (query) => async (dispatch, getState) => {
  dispatch(recentGetListLoading())
  const response = await get('/auth/me/recently', { params: query })
  dispatch(recentGetListSuccess(response))
}

export const { recentGetListLoading, recentGetListSuccess, recentGetListFail } =
  recentSlice.actions

const recentReducer = recentSlice.reducer

export default recentReducer
