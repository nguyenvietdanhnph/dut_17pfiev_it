import { createSlice } from '@reduxjs/toolkit'
import { get } from 'api/axiosClient'
import {
  createArrayInitialState,
  createArrayNoPaginateReducer,
} from 'utils/utilRedux'

const searchSlice = createSlice({
  name: 'search',
  initialState: {
    loading: false,
    data: [],
    metadata: {
      count: 10,
      total: 0,
      page: 1,
      pageCount: 1,
    },
    error: null,
    searchValue: '',
  },
  reducers: {
    ...createArrayNoPaginateReducer('searchGetList'),
    setSearchValue: (state, action) => {
      state.searchValue = '' + action.payload
    },
  },
})

export const searchGetList = (payload) => async (dispatch, getState) => {
  dispatch(searchGetListLoading())
  dispatch(setSearchValue(payload))
  const objectFilter = []
  objectFilter.push({ name: { $contL: payload } })
  objectFilter.push({ 'tags.name': { $contL: payload } })
  objectFilter.push({ 'user.profile.name': { $contL: payload } })
  objectFilter.push({ 'address.description': { $contL: payload } })
  const s = {
    $or: objectFilter,
  }
  const response = await get('/jobs', { params: { s, limit: 9999 } })
  dispatch(searchGetListSuccess(response))
}

export const refreshSearch = () => async (dispatch, getState) => {
  const state = getState()
  await dispatch(searchGetList(state.search.searchValue))
}

export const {
  searchGetListLoading,
  searchGetListSuccess,
  searchGetListFail,
  setSearchValue,
} = searchSlice.actions

const searchReducer = searchSlice.reducer

export default searchReducer
