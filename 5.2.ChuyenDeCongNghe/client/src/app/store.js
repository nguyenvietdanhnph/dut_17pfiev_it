import { configureStore } from '@reduxjs/toolkit'
import authReducer from 'features/Auth/authSlice'
import snackbarReducer from 'components/CustomSnackBar/snackbarSlice'
import dialogReducer from 'components/ConfirmDialog/dialogSlice'
import userReducer from 'features/User/userSlice'
import jobDetailsReducer from 'features/User/components/JobDetails/jobDetailsSlice'
import exploreReducer from './exploreSlice'
import storage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist'
import { combineReducers } from 'redux'
import recentReducer from './recentSlice'
import favoriteReducer from './favoriteSlice'
import searchReducer from './searchSlice'

const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['auth'],
}

const reducers = combineReducers({
  auth: authReducer,
  snackbar: snackbarReducer,
  dialog: dialogReducer,
  user: userReducer,
  jobDetails: jobDetailsReducer,
  explore: exploreReducer,
  recent: recentReducer,
  favorite: favoriteReducer,
  search: searchReducer,
})

const persistedReducer = persistReducer(persistConfig, reducers)

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
})

let persistor = persistStore(store)

export { store, persistor }
