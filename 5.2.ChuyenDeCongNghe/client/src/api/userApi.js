import axiosClient from 'api/axiosClient'
import URL from 'constants/api'

function login(data) {
  return axiosClient.post(URL.loginUrl, data)
}

function addJob(data) {
  return axiosClient.post(URL.addJob, data)
}

function getAllTag() {
  return axiosClient.get(URL.getAllTag)
}

function getJobByUserId(data) {
  return axiosClient.get(URL.addJob + '?filter=user.id||$eq||' + data)
}

function getProfile() {
  return axiosClient.get(URL.getProfile)
}

export default { login, addJob, getAllTag, getJobByUserId, getProfile }
