import { Route, Switch } from 'react-router-dom'

import { PrivateRoute, PrivateRouteUser } from 'utils/PrivateRoute'
import UserLogin from 'features/Auth/components/AuthUser/UserLogin'
import UserSignup from 'features/Auth/components/AuthUser/UserSignup'
import NavBar from 'features/User/components/NavBar/NavBar'
import HomeScreen from 'features/User/screens/HomeScreen'

function App() {
  return (
    <div>
      {/* <Header /> */}
      <Switch>
        <Route path="/login" exact component={UserLogin} />
        <Route path="/signup" exact component={UserSignup} />
        <Route path="/" exact component={NavBar} />
        <PrivateRouteUser path="/home" component={HomeScreen} />
        <Route path="/contributor" exact component={HomeScreen} />
      </Switch>
    </div>
  )
}

export default App
