export function createArrayReducer(name) {
  const result = {}

  result[`${name}Loading`] = (state, action) => {
    state['loading'] = true
    state['error'] = null
  }
  result[`${name}Success`] = (state, action) => {
    const metadata = action.payload.data
    const currentPage = action.payload.data.page
    const dataGet = action.payload.data.data

    const data =
      currentPage === 1 || !currentPage
        ? dataGet
        : state['data'].concat(
            dataGet.filter((item) => state['data'].indexOf(item) < 0)
          )
    state['loading'] = false
    state['data'] = data
    state['metadata'] = metadata
    state['error'] = null
  }
  result[`${name}Fail`] = (state, action) => {
    const error = action.payload
    state['loading'] = false
    state['error'] = error
  }

  return result
}

export function createArrayNoPaginateReducer(name) {
  const result = {}

  result[`${name}Loading`] = (state, action) => {
    state['loading'] = true
    state['error'] = null
  }
  result[`${name}Success`] = (state, action) => {
    const metadata = action.payload.data
    const dataGet = action.payload.data

    state['loading'] = false
    state['data'] = dataGet
    state['metadata'] = metadata
    state['error'] = null
  }
  result[`${name}Fail`] = (state, action) => {
    const error = action.payload
    state['loading'] = false
    state['error'] = error
  }

  return result
}

export function createArrayInitialState() {
  const result = {
    loading: false,
    data: [],
    metadata: {
      count: 10,
      total: 0,
      page: 1,
      pageCount: 1,
    },
    error: null,
  }

  return result
}

export default { createArrayInitialState, createArrayReducer }
