import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { get, post } from 'api/axiosClient'

export const fetchJobDetailNoUpdate = (jobId) => async (dispatch) => {
  dispatch(jobDetail_loading())
  const response = await get(`/jobs/getOne/${jobId}`)
  dispatch(jobDetail_success(response.data))
}

export const fetchJobDetail = (jobId) => async (dispatch) => {
  dispatch(jobDetail_loading())
  const response = await get(`/jobs/getOne/recently/${jobId}`)
  dispatch(jobDetail_success(response.data))
}

export const favorite = createAsyncThunk(
  'jobDetail/favorite',
  async (jobId, thunkAPI) => {
    const response = await post(`/jobs/${jobId}/favorites`)
    thunkAPI.dispatch(fetchJobDetailNoUpdate(jobId))
    return response
  }
)

const jobDetailsSlice = createSlice({
  name: 'jobDetails',
  initialState: {
    loading: false,
    onSubmit: undefined,
    type: '',
    status: '',
    errorMessage: '',
    data: {},
  },
  reducers: {
    setType: (state, action) => {
      state.type = action.payload
    },
    showForm: (state, action) => {
      state.onSubmit = action.payload.onSubmit
      return state
    },
    closeForm: (state) => {
      state.onSubmit = undefined
      return state
    },
    jobDetail_loading(state) {
      state.loading = true
    },
    jobDetail_success(state, action) {
      state.loading = false
      state.data = action.payload
    },
    jobDetail_error(state, action) {
      state.loading = false
      state.error = action.payload
    },
  },
  extraReducers: (builder) => {
    builder.addCase(favorite.fulfilled, (state) => {
      state.loading = false
    })
    builder.addCase(favorite.pending, (state) => {
      state.loading = true
    })
    builder.addCase(favorite.rejected, (state, action) => {
      state.loading = false
      state.error = action.error
    })
  },
})

const { actions, reducer } = jobDetailsSlice

export const jobDetailsSelector = (state) => state.jobDetails

export const {
  setType,
  showForm,
  closeForm,
  jobDetail_loading,
  jobDetail_success,
  jobDetail_error,
} = actions

export default reducer
