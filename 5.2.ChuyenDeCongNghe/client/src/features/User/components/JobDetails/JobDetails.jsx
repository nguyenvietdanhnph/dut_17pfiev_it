import {
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  IconButton,
  Slide,
  Chip,
  Typography,
  CardMedia,
  Box,
} from '@material-ui/core'
import { MdLocationPin } from 'react-icons/md'
import React from 'react'
import Parser from 'html-react-parser'
import { RiCloseCircleFill } from 'react-icons/ri'
import { useDispatch, useSelector } from 'react-redux'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { authSelector } from 'features/Auth/authSlice'
import { jobDetailsSelector, closeForm, favorite } from './jobDetailsSlice'
import moment from 'moment'
import FavoriteIcon from '@material-ui/icons/Favorite'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder'

function SlideTransition(props) {
  return <Slide {...props} direction="up" timeout={200} />
}

function JobDetails() {
  const dispatch = useDispatch()
  const currentJob = useSelector((state) => state.jobDetails.data)
  const { onSubmit } = useSelector(jobDetailsSelector)
  const { cityList } = useSelector(authSelector)
  const isFavorite = useSelector((state) => state.jobDetails.data?.isFavorite)

  return (
    <div>
      <Dialog
        maxWidth="md"
        fullWidth
        open={Boolean(onSubmit)}
        onClose={() => {
          dispatch(closeForm())
        }}
        style={{
          backgroundColor: 'rgba(0, 0, 0, 0.01)',
        }}
        PaperProps={{
          style: {
            backgroundColor: 'white',
            boxShadow: 'none',
            padding: 8,
            borderRadius: 17,
            height: '85vh',
            paddingLeft: 0,
            paddingRight: 0,
          },
        }}
        TransitionComponent={SlideTransition}
      >
        <Box position="absolute" top={10} right={10}>
          <IconButton
            onClick={() => {
              dispatch(closeForm())
            }}
          >
            <RiCloseCircleFill fontSize={24} />
          </IconButton>
        </Box>
        <DialogTitle>Job Details</DialogTitle>
        <Divider />
        <DialogContent style={{ padding: 0 }}>
          <Grid
            container
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <CardMedia
              component="img"
              image={currentJob?.introImg}
              style={{ height: '20vh', objectFit: 'cover' }}
            />
            <Grid
              container
              style={{
                display: 'flex',
                flexDirection: 'column',
                textAlign: 'left',
              }}
            >
              <Grid
                item
                container
                justifyContent="space-between"
                alignItems="center"
              >
                <Chip
                  size="medium"
                  label={currentJob?.type}
                  style={{
                    fontSize: 12,
                    backgroundColor: '#64b5f6',
                    color: '#fff',
                    fontWeight: 400,
                    width: 100,
                    marginTop: 16,
                    marginLeft: 24,
                  }}
                />
                <IconButton
                  style={{ marginRight: 20 }}
                  onClick={(event) => {
                    event.stopPropagation()
                    console.log('click trong')
                    dispatch(favorite(currentJob?.id))
                  }}
                >
                  {isFavorite ? (
                    <FavoriteIcon
                      fontSize="large"
                      style={{ color: '#b3296b' }}
                    />
                  ) : (
                    <FavoriteBorderIcon
                      fontSize="large"
                      style={{ color: '#b3296b' }}
                    />
                  )}
                </IconButton>
              </Grid>
              <Typography>
                <Box
                  fontSize={28}
                  fontWeight={600}
                  style={{
                    marginTop: 16,
                    marginLeft: 24,
                    color: '#7367f0',
                  }}
                >
                  {currentJob?.name}
                </Box>
              </Typography>
              <Grid
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 8,
                  marginLeft: 24,
                }}
              >
                <MdLocationPin fontSize={13} style={{ color: 'grey' }} />
                <Typography component="div">
                  <Box
                    fontSize={14}
                    fontWeight={500}
                    style={{ marginLeft: 8, color: '#5e5873' }}
                  >
                    {currentJob?.address?.description}
                  </Box>
                </Typography>
              </Grid>
              <Grid style={{ marginLeft: 16, marginTop: 8, marginBottom: 8 }}>
                {currentJob?.tags?.map((item) => (
                  <Chip
                    size="small"
                    label={item?.name}
                    style={{ marginLeft: 8 }}
                  />
                ))}
              </Grid>
              <Grid style={{ display: 'flex', flexDirection: 'row' }}>
                <Grid container style={{ alignItems: 'center' }}>
                  <Typography>
                    <Box
                      fontSize={15}
                      fontWeight={700}
                      style={{
                        marginLeft: 24,
                        color: '#5e5873',
                      }}
                    >
                      Posted:
                    </Box>
                  </Typography>
                  <Typography>
                    <Box
                      fontSize={15}
                      fontWeight={500}
                      style={{
                        marginLeft: 24,
                        color: '#5e5873',
                      }}
                    >
                      {`${moment(currentJob?.createdat).format(
                        'hh:mm A - DD/MM/YYYY'
                      )}`}
                    </Box>
                  </Typography>
                </Grid>
                <Grid container style={{ alignItems: 'center' }}>
                  <Typography>
                    <Box
                      fontSize={15}
                      fontWeight={700}
                      style={{
                        marginLeft: 24,
                        color: '#5e5873',
                      }}
                    >
                      Deadline:
                    </Box>
                  </Typography>
                  <Typography>
                    <Box
                      fontSize={15}
                      fontWeight={500}
                      style={{
                        marginLeft: 24,
                        color: '#5e5873',
                      }}
                    >
                      {`${moment(currentJob?.deadline).format('DD/MM/YYYY')} (${
                        moment(currentJob?.deadline).diff(moment(), 'days') > 0
                          ? moment(currentJob?.deadline).diff(
                              moment(),
                              'days'
                            ) + ' days left)'
                          : '(Expired)'
                      }
                    `}
                    </Box>
                  </Typography>
                </Grid>
              </Grid>
              <Divider style={{ marginTop: 24 }} />
              <Typography>
                <Box
                  fontSize={20}
                  fontWeight={700}
                  style={{
                    marginTop: 16,
                    marginLeft: 24,
                    color: '#5e5873',
                  }}
                >
                  Job Details
                </Box>
              </Typography>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',
                      width: 160,
                    }}
                  >
                    Type
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {currentJob?.type}
                  </Box>
                </Typography>
              </Grid>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',
                      width: 160,
                    }}
                  >
                    Experience
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {currentJob?.experience + ' years'}
                  </Box>
                </Typography>
              </Grid>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',
                      width: 160,
                    }}
                  >
                    Lowest wage
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {'$' + currentJob?.lowestWage}
                  </Box>
                </Typography>
              </Grid>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',
                      width: 160,
                    }}
                  >
                    Highest wage
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {'$' + currentJob?.highestWage}
                  </Box>
                </Typography>
              </Grid>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',
                      width: 160,
                    }}
                  >
                    Tags
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {String(currentJob?.tags?.map((item) => ` ${item?.name}`))}
                  </Box>
                </Typography>
              </Grid>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',

                      width: 160,
                    }}
                  >
                    Deadline
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {moment(currentJob?.deadline).format('DD/MM/YYYY')}
                  </Box>
                </Typography>
              </Grid>
              <Divider style={{ marginTop: 24 }} />
              <Typography>
                <Box
                  fontSize={20}
                  fontWeight={700}
                  style={{
                    marginTop: 32,
                    marginLeft: 24,
                    color: '#5e5873',
                  }}
                >
                  Address
                </Box>
              </Typography>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',
                      width: 160,
                    }}
                  >
                    City
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {
                      cityList?.find(
                        (item) =>
                          item?.province_id ===
                          String(currentJob?.address?.city)
                      )?.province_name
                    }
                  </Box>
                </Typography>
              </Grid>
              <Grid container>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={600}
                    style={{
                      marginTop: 16,
                      marginLeft: 32,
                      color: '#5e5873',
                      width: 160,
                    }}
                  >
                    Detail
                  </Box>
                </Typography>
                <Typography>
                  <Box
                    fontSize={15}
                    fontWeight={400}
                    style={{
                      marginLeft: 24,
                      color: '#5e5873',
                      marginTop: 16,
                    }}
                  >
                    {currentJob?.address?.description}
                  </Box>
                </Typography>
              </Grid>
              <Divider style={{ marginTop: 24 }} />
              <Typography>
                <Box
                  fontSize={20}
                  fontWeight={700}
                  style={{
                    marginTop: 32,
                    marginLeft: 24,
                    color: '#5e5873',
                  }}
                >
                  Description
                </Box>
              </Typography>

              <Typography>
                <Box
                  fontSize={15}
                  fontWeight={400}
                  style={{
                    marginLeft: 24,
                    color: '#5e5873',
                    marginTop: 16,
                  }}
                >
                  {Parser(currentJob?.description + '')}
                </Box>
              </Typography>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </div>
  )
}

export default JobDetails
