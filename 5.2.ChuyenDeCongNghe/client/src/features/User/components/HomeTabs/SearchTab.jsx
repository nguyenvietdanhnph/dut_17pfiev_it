import React from 'react'
import { useSelector } from 'react-redux'
import JobCard from '../JobCard'
import InfiniteScroll from 'react-infinite-scroll-component'

function SearchTab() {
  const jobList = useSelector((state) => state.search.data)

  return (
    <InfiniteScroll
      dataLength={jobList.length}
      hasMore={false}
      loader={<h4>Loading...</h4>}
      endMessage={
        <p style={{ textAlign: 'center' }}>
          <b>Yay! You have seen it all</b>
        </p>
      }
      style={{
        overflow: 'visible',
      }}
    >
      {jobList?.map((item) => JobCard(item))}
    </InfiniteScroll>
  )
}

export default SearchTab
