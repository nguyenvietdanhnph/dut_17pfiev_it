import React from 'react'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { exploreGetList } from 'app/exploreSlice'
import JobCard from '../JobCard'
import InfiniteScroll from 'react-infinite-scroll-component'

function ExploreTab() {
  const dispatch = useDispatch()

  const jobList = useSelector((state) => state.explore.data)
  const page = useSelector((state) => state.explore.metadata.page)
  const pageCount = useSelector((state) => state.explore.metadata.pageCount)

  useEffect(() => {
    dispatch(exploreGetList({ offset: 0, limit: 10, page: 1 }))
    console.log('mount 1 lan')
  }, [])

  const fetchMoreData = () => {
    dispatch(exploreGetList({ offset: 0, limit: 10, page: page + 1 }))
  }

  return (
    <InfiniteScroll
      dataLength={jobList.length}
      next={fetchMoreData}
      hasMore={page < pageCount}
      loader={<h4>Loading...</h4>}
      endMessage={
        <p style={{ textAlign: 'center' }}>
          <b>Yay! You have seen it all</b>
        </p>
      }
      style={{
        overflow: 'visible',
      }}
    >
      {jobList?.map((item) => JobCard(item))}
    </InfiniteScroll>
  )
}

export default ExploreTab
