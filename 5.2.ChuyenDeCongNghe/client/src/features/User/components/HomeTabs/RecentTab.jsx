import React from 'react'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { recentGetList } from 'app/recentSlice'
import JobCard from '../JobCard'
import InfiniteScroll from 'react-infinite-scroll-component'

function RecentTab() {
  const dispatch = useDispatch()

  const jobList = useSelector((state) => state.recent.data)

  useEffect(() => {
    fetchData()
    console.log('mount 1 lan')
  }, [])

  const fetchData = () => {
    dispatch(recentGetList())
  }

  return (
    <InfiniteScroll
      dataLength={jobList.length}
      next={fetchData}
      hasMore={false}
      loader={<h4>Loading...</h4>}
      endMessage={
        <p style={{ textAlign: 'center' }}>
          <b>Yay! You have seen it all</b>
        </p>
      }
      style={{
        overflow: 'visible',
      }}
    >
      {jobList?.map((item) => JobCard(item))}
    </InfiniteScroll>
  )
}

export default RecentTab
