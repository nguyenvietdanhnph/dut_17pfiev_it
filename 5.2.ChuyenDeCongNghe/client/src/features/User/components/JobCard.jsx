import React from 'react'
import {
  Box,
  ButtonBase,
  CardMedia,
  Chip,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core'
import { MdLocationPin, MdMoreHoriz } from 'react-icons/md'
import moment from 'moment'
import { useDispatch } from 'react-redux'
import { fetchJobDetail, showForm } from './JobDetails/jobDetailsSlice'

const classes = {
  item: {
    textAlign: 'initial',
    width: '100%',
    padding: 20,
    position: 'relative',
    boxShadow:
      'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
    backgroundColor: '#fff',
    borderRadius: 10,
    marginBottom: 16,
  },
}

const JobCard = (item) => {
  const dispatch = useDispatch()
  return (
    <ButtonBase
      onClick={() => {
        console.log('cick ngoai')
        dispatch(fetchJobDetail(item?.id))
        dispatch(showForm({ onSubmit: item?.id }))
      }}
      style={classes.item}
      key={item?.id}
    >
      <Grid style={{ width: '100%' }}>
        <Chip
          size="small"
          label={item?.type}
          style={{
            fontSize: 10,
            backgroundColor: '#64b5f6',
            color: '#fff',
            fontWeight: 400,
          }}
        />
        <IconButton
          style={{ position: 'absolute', top: 10, right: 10 }}
          id="fade-button"
          aria-controls="fade-menu"
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={(event) => {
            event.stopPropagation()
            console.log('click trong')
          }}
        >
          <MdMoreHoriz fontSize={24} />
        </IconButton>
        <Typography component="div">
          <Box
            fontSize={20}
            fontWeight={700}
            style={{ color: '#5e5873', marginTop: 16 }}
          >
            {item?.name}
          </Box>
        </Typography>

        <Grid
          container
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 16,
          }}
        >
          <MdLocationPin fontSize={13} style={{ color: 'grey' }} />
          <Typography component="div">
            <Box
              fontSize={14}
              fontWeight={500}
              style={{ marginLeft: 8, color: '#5e5873' }}
            >
              {item?.address?.description}
            </Box>
          </Typography>
        </Grid>
        {item?.tags?.map((item) => (
          <Chip size="small" label={item?.name} style={{ marginLeft: 8 }} />
        ))}
        <CardMedia
          component="img"
          image={item?.introImg}
          style={{
            height: 300,
            width: '100%',
            marginTop: 24,
            resizeMode: 'contain',
          }}
        />
        <Grid
          container
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 24,
          }}
        >
          <Typography component="div">
            <Box
              fontSize={14}
              fontWeight={500}
              style={{ marginLeft: 8, color: '#5e5873' }}
            >
              {`Posted: ${moment(item?.createdat).format('DD/MM/YYYY')}`}
            </Box>
          </Typography>
          <Typography component="div">
            <Box
              fontSize={14}
              fontWeight={500}
              style={{ marginLeft: 8, color: '#5e5873' }}
            >
              {`Deadline: ${moment(item?.deadline).format('DD/MM/YYYY')}`}
            </Box>
          </Typography>
        </Grid>
      </Grid>
    </ButtonBase>
  )
}

export default JobCard
