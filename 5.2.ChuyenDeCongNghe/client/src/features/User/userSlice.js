import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import userApi from 'api/userApi'

export const addJob = createAsyncThunk(
  'user/addjob',
  async (payload, { rejectWithValue }) => {
    try {
      const response = await userApi.addJob(payload)
      return response.data
    } catch (error) {
      return rejectWithValue(error.response.data)
    }
  }
)

export const getAllTag = createAsyncThunk(
  'user/getalltag',
  async (payload, { rejectWithValue }) => {
    try {
      const response = await userApi.getAllTag()
      return response.data
    } catch (error) {
      return rejectWithValue(error.response.data)
    }
  }
)

export const getJobByUserId = createAsyncThunk(
  'user/getjobbyuserid',
  async (payload, { rejectWithValue }) => {
    try {
      const response = await userApi.getJobByUserId(payload)
      return response.data
    } catch (error) {
      return rejectWithValue(error.response.data)
    }
  }
)
const userSlice = createSlice({
  name: 'user',
  initialState: {
    status: '',
    file: '',
    errorMessage: '',
    jobList: [],
    tagList: [],
  },
  reducers: {
    clearState: (state) => {
      state.status = ''
      return state
    },
  },
  extraReducers: {
    [addJob.pending]: (state) => {
      state.status = 'addJob.pending'
    },
    [addJob.fulfilled]: (state, { payload }) => {
      state.status = 'addJob.fulfilled'
    },
    [addJob.rejected]: (state, { payload }) => {
      state.status = 'addJob.rejected'
      state.errorMessage = payload.message
    },
    [getAllTag.pending]: (state) => {
      state.status = 'getAllTag.pending'
    },
    [getAllTag.fulfilled]: (state, { payload }) => {
      state.status = 'getAllTag.fulfilled'
      state.tagList = payload
    },
    [getAllTag.rejected]: (state, { payload }) => {
      state.status = 'getAllTag.rejected'
      state.errorMessage = payload.message
    },
    [getJobByUserId.pending]: (state) => {
      state.status = 'getJobByUserId.pending'
    },
    [getJobByUserId.fulfilled]: (state, { payload }) => {
      state.status = 'getJobByUserId.fulfilled'
      state.jobList = payload
      console.log(state)
    },
    [getJobByUserId.rejected]: (state, { payload }) => {
      state.status = 'getJobByUserId.rejected'
      state.errorMessage = payload.message
    },
  },
})

const { actions, reducer } = userSlice

export const userSelector = (state) => state.user

export const { clearState } = actions

export default reducer
