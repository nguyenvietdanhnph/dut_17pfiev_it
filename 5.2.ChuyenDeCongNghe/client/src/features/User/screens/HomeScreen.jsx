import React from 'react'
import {
  AppBar,
  Avatar,
  Badge,
  Box,
  Button,
  CardMedia,
  Container,
  Grid,
  IconButton,
  ListItemIcon,
  Menu,
  Tab,
  Tabs,
  Toolbar,
  Typography,
  withStyles,
  Divider,
} from '@material-ui/core'
import LogoSVG from 'assets/logo.svg'
import { BiMessage } from 'react-icons/bi'
import { RiNotification2Line } from 'react-icons/ri'
import { authSelector, logout, getProfile } from 'features/Auth/authSlice'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import { MdLogout, MdOutlineAccountCircle } from 'react-icons/md'
import { useHistory } from 'react-router-dom'
import ContributorBG from 'assets/ProfileBackground.jpg'
import sidePhoto from 'assets/sidePhoto.jpg'
import moment from 'moment'
import ExploreTab from '../components/HomeTabs/ExploreTab'
import JobDetails from '../components/JobDetails/JobDetails'
import RecentTab from '../components/HomeTabs/RecentTab'
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'
import { alpha, makeStyles } from '@material-ui/core/styles'
import FavoriteTab from '../components/HomeTabs/FavoriteTab'
import RefreshIcon from '@material-ui/icons/Refresh'
import { exploreGetList } from 'app/exploreSlice'
import { recentGetList } from 'app/recentSlice'
import { favoriteGetList } from 'app/favoriteSlice'
import SearchTab from '../components/HomeTabs/SearchTab'
import { refreshSearch, searchGetList } from 'app/searchSlice'

const StyledMenuItem = withStyles({
  root: {
    '&:hover': {
      backgroundColor: 'rgba(115, 103, 240, 0.15)',
      color: 'rgb(115, 103, 240)',
    },
    '& .MuiListItemIcon-root:hover': {
      color: 'rgb(115, 103, 240)',
    },
  },
})(MenuItem)

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

function TabPanel(props) {
  const { children, value, index, ...other } = props
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

function HomeScreen(props) {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const [value, setValue] = React.useState(0)

  const classes = useStyles()
  const dispatch = useDispatch()
  const history = useHistory()
  const { info } = useSelector(authSelector)

  useEffect(() => {
    dispatch(getProfile())
  }, [])

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  const handleLogout = () => {
    dispatch(logout())
    setAnchorEl(null)
    history.push('/login')
  }
  const handleSubmit = (event) => {
    event.preventDefault()
    const searchValue = event.target.elements.search.value
    console.log('search', searchValue)
    dispatch(searchGetList(searchValue))
    setValue(3)
  }
  const onRefresh = () => {
    switch (value) {
      case 0:
        dispatch(exploreGetList({ offset: 0, limit: 10, page: 1 }))
        break
      case 1:
        dispatch(recentGetList())
        break
      case 2:
        dispatch(favoriteGetList())
        break
      case 3:
        dispatch(refreshSearch())
        break
      default:
    }
    console.log('refresh')
  }

  return (
    <>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <Grid container justifyContent="space-between" alignItems="center">
            <Grid item>
              <Grid
                container
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <CardMedia image={LogoSVG} style={{ width: 35, height: 25 }} />
                <Typography>
                  <Box
                    fontSize={20}
                    fontWeight={500}
                    style={{ marginLeft: 10, color: '#fff' }}
                  >
                    IT Network
                  </Box>
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <div className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <form className={classes.container} onSubmit={handleSubmit}>
                  <InputBase
                    name="search"
                    placeholder="Search…"
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput,
                    }}
                    inputProps={{ 'aria-label': 'search' }}
                  />
                </form>
              </div>
            </Grid>
            <Grid item>
              <Grid container alignItems="center">
                <IconButton
                  size="medium"
                  edge="start"
                  aria-label="menu"
                  sx={{ mr: 2 }}
                  style={{ marginRight: 15 }}
                  color="inherit"
                >
                  <Badge
                    badgeContent={5}
                    style={{ marginRight: 10 }}
                    color="secondary"
                  >
                    <BiMessage fontSize={20} />
                  </Badge>
                </IconButton>
                <IconButton
                  size="medium"
                  edge="start"
                  aria-label="menu"
                  sx={{ mr: 2 }}
                  style={{ marginRight: 15 }}
                  color="inherit"
                >
                  <Badge
                    badgeContent={3}
                    style={{ marginRight: 10 }}
                    color="secondary"
                    f
                  >
                    <RiNotification2Line fontSize={20} />
                  </Badge>
                </IconButton>
                {info && (
                  <div style={{ marginRight: 15 }}>
                    <Typography component="div">
                      <Box fontSize={15} fontWeight={600}>
                        {info?.profile?.name}
                      </Box>
                    </Typography>
                    <Typography component="div">
                      <Box
                        fontSize={12}
                        fontWeight={300}
                        style={{ color: 'grey', textTransform: 'capitalize' }}
                      >
                        {info?.role?.role}
                      </Box>
                    </Typography>
                  </div>
                )}
                <IconButton
                  size="medium"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleMenu}
                >
                  <Badge
                    classes={{ badge: classes.customBadge }}
                    variant="dot"
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                  >
                    <Avatar src={info?.profile?.profileUrl} />
                  </Badge>
                </IconButton>
                <Menu
                  className={classes.menu}
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  PaperProps={{
                    elevation: 0,
                    style: {
                      boxShadow:
                        'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
                      width: 180,
                    },
                  }}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <StyledMenuItem onClick={handleClose}>
                    <ListItemIcon>
                      <MdOutlineAccountCircle fontSize={18} />
                    </ListItemIcon>
                    <Typography component="div" style={{ marginLeft: -18 }}>
                      <Box fontSize={15}>Profile</Box>
                    </Typography>
                  </StyledMenuItem>

                  <StyledMenuItem onClick={handleLogout}>
                    <ListItemIcon>
                      <MdLogout fontSize={18} />
                    </ListItemIcon>
                    <Typography component="div" style={{ marginLeft: -18 }}>
                      <Box fontSize={15}>Logout</Box>
                    </Typography>
                  </StyledMenuItem>
                </Menu>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Box
        style={{
          backgroundImage: `linear-gradient(to top, rgb(255, 255, 255), rgb(255, 255, 255), rgba(11, 40, 5, 0.6))`,
        }}
      >
        <Container>
          <Grid item xs className={classes.imageContainer}>
            <CardMedia
              image={ContributorBG}
              style={{
                width: '100%',
                height: '100%',
                boxShadow:
                  'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
              }}
            />
            <Grid
              style={{
                position: 'absolute',
                bottom: 40,
                left: 32,
                width: 120,
                height: 120,
                backgroundColor: '#fff',
                elevation: 10,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
                boxShadow:
                  'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
              }}
            >
              <CardMedia
                image={info?.profile?.profileUrl}
                style={{
                  width: 112,
                  height: 112,
                  borderRadius: 10,
                }}
              />
            </Grid>
            <Grid style={{ position: 'absolute', bottom: 80, left: 184 }}>
              <Typography component="div">
                <Box fontSize={24} fontWeight={500} style={{ color: '#fff' }}>
                  {info?.profile?.name}
                </Box>
              </Typography>
              <Typography component="div">
                <Box fontSize={13} fontWeight={300} style={{ color: '#fff' }}>
                  {info?.role?.role}
                </Box>
              </Typography>
            </Grid>
          </Grid>
          <Grid className={classes.spaceTabPanel} />
        </Container>
        <Divider />
      </Box>
      <Container className={classes.root}>
        <Grid className={classes.tabPanel}>
          <Divider />
          <Grid item>
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              <Tab
                label="Explore"
                {...a11yProps(0)}
                style={{
                  height: 64,
                  fontSize: 15,
                  textTransform: 'capitalize',
                }}
              />
              <Tab
                label="Recent"
                {...a11yProps(1)}
                style={{
                  height: 64,
                  fontSize: 15,
                  textTransform: 'capitalize',
                }}
              />
              <Tab
                label="Favorite"
                {...a11yProps(2)}
                style={{
                  height: 64,
                  fontSize: 15,
                  textTransform: 'capitalize',
                }}
              />
              <Tab
                label="Search"
                {...a11yProps(3)}
                style={{
                  height: 64,
                  fontSize: 15,
                  textTransform: 'capitalize',
                }}
              />
            </Tabs>
          </Grid>
          <IconButton className={classes.refreshButton} onClick={onRefresh}>
            <RefreshIcon />
          </IconButton>
          <Button className={classes.button} onClick={() => {}}>
            POST NEW JOB
          </Button>
          <Divider />
        </Grid>

        <Grid item style={{ display: 'flex', flexDirection: 'row' }}>
          <Grid item sm={3} container className={classes.about}>
            <Grid>
              <Typography component="div">
                <Box
                  fontSize={18}
                  fontWeight={700}
                  style={{ color: '#5e5873' }}
                >
                  Introduction
                </Box>
              </Typography>
              <Typography component="div">
                <Box
                  fontSize={13}
                  fontWeight={400}
                  style={{ color: '#5e5873', marginTop: 24 }}
                >
                  {info?.profile?.introduction}
                </Box>
              </Typography>
            </Grid>
            <Grid style={{ marginTop: 20 }}>
              <Typography component="div">
                <Box
                  fontSize={15}
                  fontWeight={600}
                  style={{ color: '#5e5873' }}
                >
                  Joined
                </Box>
              </Typography>
              <Typography component="div">
                <Box
                  fontSize={13}
                  fontWeight={400}
                  style={{ color: '#5e5873', marginTop: 6 }}
                >
                  {moment(info?.createdat).format('MMMM DD, YYYY')}
                </Box>
              </Typography>
            </Grid>
            <Grid style={{ marginTop: 20 }}>
              <Typography component="div">
                <Box
                  fontSize={15}
                  fontWeight={600}
                  style={{ color: '#5e5873' }}
                >
                  City
                </Box>
              </Typography>
              <Typography component="div">
                <Box
                  fontSize={13}
                  fontWeight={400}
                  style={{ color: '#5e5873', marginTop: 6 }}
                >
                  {info?.profile?.city}
                </Box>
              </Typography>
            </Grid>
            <Grid style={{ marginTop: 20 }}>
              <Typography component="div">
                <Box
                  fontSize={15}
                  fontWeight={600}
                  style={{ color: '#5e5873' }}
                >
                  Email
                </Box>
              </Typography>
              <Typography component="div">
                <Box
                  fontSize={13}
                  fontWeight={400}
                  style={{ color: '#5e5873', marginTop: 6 }}
                >
                  {info?.email}
                </Box>
              </Typography>
            </Grid>
            <Grid style={{ marginTop: 20 }}>
              <Typography component="div">
                <Box
                  fontSize={15}
                  fontWeight={600}
                  style={{ color: '#5e5873' }}
                >
                  Website
                </Box>
              </Typography>
              <Typography component="div">
                <Box
                  fontSize={13}
                  fontWeight={400}
                  style={{ color: '#5e5873', marginTop: 6 }}
                >
                  {info?.profile?.pageURL}
                </Box>
              </Typography>
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <TabPanel value={value} index={0}>
              <ExploreTab />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <RecentTab />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <FavoriteTab />
            </TabPanel>
            <TabPanel value={value} index={3}>
              <SearchTab />
            </TabPanel>
          </Grid>
          <Grid
            item
            xs={3}
            className={classes.about}
            style={{ padding: 0, overflow: 'hidden' }}
          >
            <CardMedia
              image={sidePhoto}
              style={{
                width: '100%',
                height: '100%',
                boxShadow:
                  'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
              }}
            />
          </Grid>
        </Grid>
        <JobDetails />
      </Container>
    </>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: -theme.spacing(8) - 2,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    height: theme.spacing(8),
    color: '#fff',
    boxShadow:
      'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
  },

  menu: {
    marginTop: theme.spacing(7),
  },
  imageContainer: {
    overflow: 'hidden',
    backgroundColor: '#fff',
    marginTop: theme.spacing(8),
    height: theme.spacing(60),
    borderRadius: 10,
    position: 'relative',
    boxShadow:
      'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
  },
  button: {
    position: 'absolute',
    bottom: 14,
    right: 16,
    backgroundColor: '#7367f0',
    color: '#fff',
    borderRadius: 10,
    boxShadow: 'none',
    width: theme.spacing(16),
    height: theme.spacing(5),
    fontSize: 12,
    '&:hover': {
      boxShadow:
        'rgba(115,103,240,0.25) 0px 15px 25px, rgba(115,103,240, 0.15) 0px 5px 10px',
      background: '#7367f0',
    },
  },
  refreshButton: {
    position: 'absolute',
    bottom: theme.spacing(1),
    right: theme.spacing(20),
  },
  about: {
    position: 'sticky',
    top: theme.spacing(16),
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#fff',
    padding: theme.spacing(2.5),
    marginTop: theme.spacing(3),
    borderRadius: 10,
    height: 480,
    boxShadow:
      'rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px',
  },
  tabPanel: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: '#fff',
    position: 'sticky',
    top: theme.spacing(8),
    height: theme.spacing(8),
  },
  spaceTabPanel: {
    height: theme.spacing(8),
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}))

export default HomeScreen
