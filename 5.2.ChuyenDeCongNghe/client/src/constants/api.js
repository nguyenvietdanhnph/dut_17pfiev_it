const loginUrl = '/auth'

const getProfile = '/auth/me'

const getAllUserUrl = '/users'

const getRequestUser = '/users/unauthorized'

const getDeleteUser = '/users/inactive'

const restoreUser = '/users/restore'

const identifyUser = '/users/identify'

const getAllCategory = '/categories/all'

const getAllParent = '/categories/allWithChildren'

const addCategory = '/categories'

const getOneCategory = '/categories/getone'

const getAllJobs = '/jobs'

const getJobRequest = '/jobs/inactive/all'

const identifyJob = '/jobs/active'

const getDeletedJob = '/jobs/softdelete/all'

const deletePermanentlyJob = '/jobs/delete'

const uploadFile = '/apply/upload'

const addJob = '/jobs'

const getAllTag = '/jobs/tags/all'
export default {
  loginUrl,
  getProfile,
  getAllUserUrl,
  getRequestUser,
  getDeleteUser,
  restoreUser,
  identifyUser,
  getAllCategory,
  getAllParent,
  addCategory,
  getOneCategory,
  getAllJobs,
  getJobRequest,
  identifyJob,
  getDeletedJob,
  deletePermanentlyJob,
  uploadFile,
  addJob,
  getAllTag,
}
