module lock(clk, reset, n, unlocked);
    input clk, reset;
    input [9:0] n;
    output reg unlocked;

    reg [2:0] state, next_state;

    parameter [2:0] s_rst = 3'd0;
    parameter [2:0] s_0 = 3'd1;
    parameter [2:0] s_01 = 3'd2;
    parameter [2:0] s_010 = 3'd3;
    parameter [2:0] s_0108 = 3'd4;
    parameter [2:0] s_01089 = 3'd5;
    parameter [2:0] s_010899 = 3'd6;

    assign unlocked = (state == s_010899);

    always @(posedge clk)
        begin
            if (reset) state = s_rst;
            else state = next_state;
        end
    
    always @(*)
        begin
            case (state)
                s_rst: next_state = (n == 10{1'b0}) ? state : (n == 10'b0000000001) ? s_0 : s_rst ;
                s_0: next_state = (n == 10{1'b0}) ? state : (n == 10'b0000000010) ? s_01 : s_rst;
                s_01: next_state = (n == 10{1'b0}) ? state : (n == 10'b0000000001) ? s_010 : s_rst;
                s_010: next_state = (n == 10{1'b0}) ? state : (n == 10'b0100000000) ? s_0108 : s_rst;
                s_0108: next_state = (n == 10{1'b0}) ? state : (n == 10'b1000000000) ? s_01089 : s_rst;
                s_01089: next_state = (n == 10{1'b0}) ? state : (n == 10'b1000000000) ? s_010899 : s_rst;
                s_010899: next_state = (n == 10{1'b0}) ? state : s_rst;
                default: next_state = s_rst;
            endcase    
        end
endmodule