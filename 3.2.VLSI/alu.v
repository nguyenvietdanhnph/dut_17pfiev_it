module alu(A, B, Cin, M0, M1, OE, Y, Cout);
    input [7:0] A, B;
    input Cin, M0, M1, OE;
    output reg [7:0] Y;
    output reg Cout;
    always @(*) 
        begin
            if (!OE) Y = 8{1'bZ};
            else
                begin
                    case ({M1, M0})
                        2'b00: {Cout, Y} = A + B + Cin;
                        2'b01: {Cout, Y} = A - B - Cin;
                        2'b10: Y = A & B;
                        2'b11: Y = A | B;
                    endcase
                end
        end
endmodule    

