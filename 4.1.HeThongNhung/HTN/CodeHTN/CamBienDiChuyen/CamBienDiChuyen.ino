const int infraredPIN = 2;
const int led = 13;

void setup(){
  pinMode(led, OUTPUT);
  pinMode(infraredPIN, INPUT);
  Serial.begin(9600);
}

void loop(){
  if (digitalRead(infraredPIN)) {
    digitalWrite(led, HIGH);
    Serial.println("Co vat di chuyen");
  }
  else{
    digitalWrite(led, LOW);
    Serial.println("Khong co vat gi");
  }
  delay(1000);
}
