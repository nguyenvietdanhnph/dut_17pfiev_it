int analogValue, digitalValue;
int digitalPin = 7;
int LED = 13;

void setup() {
  pinMode(sensorPin, INPUT);
  pinMode(LED, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  analogValue = analogRead(A0);
  digitalValue = digitalRead(sensorPin);
  if (digitalValue)
    digitalWrite(LED, LOW);
  else digitalWrite(LED, HIGH);
  Serial.print("Tin hieu so: ");
  Serial.print(digitalValue);
  Serial.print("Tin hieu tuong tu: ");
  Serial.print(analogValue);
  Serial.println("");

  delay(500);
}
