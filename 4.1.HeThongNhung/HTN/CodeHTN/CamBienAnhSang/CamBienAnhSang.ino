int LED = 13;
int sensorPin = 8;
int value = 0;

void setup(){
  pinMode(LED, OUTPUT);
  pinMode(sensorPin, INPUT);
}

void loop(){
  value = digitalRead(sensorPin);
  digitalWrite(LED, value);

  delay(10);
}
