const int trigPin = 5; //PWM trigger
const int echoPin = 3; //PWM output 0 - 25000US, Every 50US represent 1 cm
long duration; // defines variables
int distance;

void setup() {
  pinMode(trigPin, OUTPUT); //cài đặt trigPin là đầu ra
  pinMode(echoPin, INPUT); //cài đặt echoPin làm đầu vào
  Serial.begin(9600); //định cấu hình tốc độ truyền lên 9600bps
}

void loop() {
  digitalWrite(trigPin, LOW); //tắt trigPin
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH); //Sets the trigPin on HIGH state for 10 micro seconds
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH); //read the echoPin, returns the sound wave           travel time in microseconds
  distance = duration * 0.034 / 2; //Tính khoảng cách
  Serial.print("Khoảng cách đo được = "); //Hiện khoảng cách ra Serial Monitor
  Serial.print(distance);
  Serial.println(" cm");
}
