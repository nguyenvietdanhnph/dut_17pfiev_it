#define BTN_DIR 13
#define POT  0
#define EN1  3
#define M1A  4
#define M1B  7

int pos, veloc, oldpos = -1;

void setup() {
  pinMode(M1A, OUTPUT);
  pinMode(M1B, OUTPUT);
  pinMode(BTN_DIR, INPUT);
}

void loop() {
  pos = analogRead(POT);
  if (pos != oldpos) {
    veloc = map(pos, 0, 1023, 0, 255);
    analogWrite(EN1, veloc);
    oldpos = pos;
  }
  if (digitalRead(BTN_DIR) == LOW) {
    digitalWrite(M1A, HIGH);
    digitalWrite(M1B, LOW);
  }
  else {
    digitalWrite(M1A, LOW);
    digitalWrite(M1B, HIGH);
  }
  delay(200);
}
