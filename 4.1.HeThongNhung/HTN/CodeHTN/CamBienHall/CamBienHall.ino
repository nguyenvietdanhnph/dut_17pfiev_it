int LED = 13;
int hallPin = 2;
int value = 0;

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(hallPin, INPUT);
}

void loop() {
  value = digitalRead(hallPin);
  if (value)
    digitalWrite(LED, LOW);
  else
    digitalWrite(LED, HIGH);
}
