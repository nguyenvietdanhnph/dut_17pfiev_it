#define flamePin A0
#define led 13

void setup() {
  Serial.begin(9600);
  pinMode(led,OUTPUT);
}

void loop() {
  Serial.print("Gia tri cam bien: ");
  int value = analogRead(flamePin);
  Serial.println(value);
  if (value<800){
    Serial.println("Co chay!");
    digitalWrite(ledPin, HIGH); 
    delay(100); 
    digitalWrite(ledPin, LOW);
    delay(100);  
  }
  else {
    digitalWrite(ledPin, LOW);
    Serial.println();
  }
  delay(100);
}
