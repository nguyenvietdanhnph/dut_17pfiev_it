int button = 11;
int led = 2;
void setup() 
{
  pinMode(button, INPUT); //Cài đặt chân D11 dưới dạng INPUT
  pinMode(led,OUTPUT);    //Cài đặt chân D2 dưới dạng OUTPUT
}

void loop() 
{
  int buttonStatus = digitalRead(button);    //Đọc trạng thái button
  if (buttonStatus == HIGH) 
  { // Nếu mà button bị nhấn
    digitalWrite(led,HIGH); // Đèn led sáng
  } else 
  { // Ngược lại
    digitalWrite(led,LOW); // Đèn led tắt
  }
}
