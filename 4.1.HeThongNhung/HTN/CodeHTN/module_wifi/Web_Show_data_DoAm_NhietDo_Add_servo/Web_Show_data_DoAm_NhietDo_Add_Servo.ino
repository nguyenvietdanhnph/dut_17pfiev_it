// Them thu vien
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "DHT.h"
#include <Servo.h>

// Pin
#define DHTPIN D5

// Su dung cam bien DHT11 
#define DHTTYPE DHT11

// Thiet lap DHT
DHT dht(DHTPIN, DHTTYPE, 15);
Servo myservo;

float h,t;
int pos=0;
// Thong so WiFi nha ban
const char* ssid = "TRO WIFI";  // Enter SSID here
const char* password = "@kiet48@tren17@";  //Enter Password here

// Tao server
ESP8266WebServer server(80);

void setup() {
  myservo.attach(D8);
  // Mo Serial
  Serial.begin(115200);
  delay(10);
  
  // Khoi tao DHT 
  dht.begin();
  // Ket noi toi mang WiFi
  Serial.println();
  Serial.println();
  Serial.print("Ket noi toi mang ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Da ket noi WiFi");
  
  
  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);
  
  // Khoi dong server
  server.begin();
  Serial.println("Khoi dong Server");
  Serial.println(WiFi.localIP());
}

void loop() {
  
  // Kiem tra khi co client ket noi
  server.handleClient();
  
  if(h>=80&&t>=30){
    servo_on();  
  } else {
      myservo.write(0);
    }
}
void servo_on(){
  for(pos = 0; pos < 180; pos += 1){ 
        myservo.write(pos);
        delay(15);
  }
    
  for(pos = 180; pos>=1; pos-=1) {                           
        myservo.write(pos);
        delay(15);
  }   
}
void handle_OnConnect(){
    // Doc do am
  h = dht.readHumidity();
  // Doc nhiet do o do C
  t = dht.readTemperature();

  Serial.print("Do am: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Nhiet do: ");
  Serial.print(t);
  Serial.println(" *C ");
  server.send(200, "text/html", SendHTML(t,h));
}
void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}
String SendHTML(float t, float h){
  String s = "\r\n\r\n";
  s += "<head>";
  s += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
  s += "<meta http-equiv=\"refresh\" content=\"1\" />";
  //s += "<script src=\"https://code.jquery.com/jquery-2.1.3.min.js\"></script>";
  //s += "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">";
  s += "<style>body{font-size: 24px;} .voffset {margin-top: 30px;}</style>";
  s += "</head>";
  
  s += "<div class=\"container\">";
  s += "<h1>Theo doi nhiet do va do am</h1>";
  s += "<div class=\"row voffset\">";
  s += "<div class=\"col-md-3\">Nhiet do: </div><div class=\"col-md-3\">" + String(t) + "</div>";
  s += "<div class=\"col-md-3\">Do am: </div><div class=\"col-md-3\">" + String(h) + "</div>";
  if(h>=80&&t>=30){
    s += "<div class=\"col-md-3\">Do am: </div><div class=\"col-md-3\">ON</div>";  
  }
  else{
    s += "<div class=\"col-md-3\">Do am: </div><div class=\"col-md-3\">OFF</div>";
  }
  s += "</div>";
  return s;
}
