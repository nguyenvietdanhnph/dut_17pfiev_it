// Them thu vien
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "DHT.h"

// Pin
#define DHTPIN D5

// Su dung cam bien DHT11 
#define DHTTYPE DHT11

// Thiet lap DHT
DHT dht(DHTPIN, DHTTYPE, 15);

float h,t;
// Thong so WiFi nha ban
const char* ssid = "pfiev";  // Enter SSID here
const char* password = "123456789";  //Enter Password here

// Tao server
ESP8266WebServer server(80);

void setup() {
  // Mo Serial
  Serial.begin(115200);
  delay(10);
  
  // Khoi tao DHT 
  dht.begin();
  // Ket noi toi mang WiFi
  Serial.println();
  Serial.println();
  Serial.print("Ket noi toi mang ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Da ket noi WiFi");
  
  
  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);
  
  // Khoi dong server
  server.begin();
  Serial.println("Khoi dong Server");
  Serial.println(WiFi.localIP());
}

void loop() {
  
  // Kiem tra khi co client ket noi
  server.handleClient();
}
void handle_OnConnect(){
    // Doc do am
  h = dht.readHumidity();
  // Doc nhiet do o do C
  t = dht.readTemperature();

  Serial.print("Do am: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Nhiet do: ");
  Serial.print(t);
  Serial.println(" *C ");
  server.send(200, "text/html", SendHTML(t,h));
}
void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}
String SendHTML(float t, float h){
  String s = "\r\n\r\n";
  s += "<head>";
  s += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
  s += "<meta http-equiv=\"refresh\" content=\"1\" />";
  //s += "<script src=\"https://code.jquery.com/jquery-2.1.3.min.js\"></script>";
  //s += "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">";
  s += "<style>body{font-size: 24px;} .voffset {margin-top: 30px;}</style>";
  s += "</head>";
  
  s += "<div class=\"container\">";
  s += "<h1>Theo doi nhiet do va do am</h1>";
  s += "<div class=\"row voffset\">";
  s += "<div class=\"col-md-3\">Nhiet do: </div><div class=\"col-md-3\">" + String(t) + "</div>";
  s += "<div class=\"col-md-3\">Do am: </div><div class=\"col-md-3\">" + String(h) + "</div>";
  s += "</div>";
  return s;
}
