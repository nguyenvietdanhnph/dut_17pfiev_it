#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

//trigPin
//echoPin
const int trigPin = D5; //PWM trigger
const int echoPin = D2; //PWM output 0 - 25000US, Every 50US represent 1 cm
long duration; // defines variables
float distance;

// Thong so WiFi nha ban
const char* ssid = "pfiev";  // Enter SSID here
const char* password = "123456789";  //Enter Password here

// Tao server
ESP8266WebServer server(80);

void setup() {
  pinMode(trigPin, OUTPUT); //cài đặt trigPin là đầu ra
  pinMode(echoPin, INPUT); //cài đặt echoPin làm đầu vào
  pinMode(D0, OUTPUT);
  Serial.begin(115200);
  delay(10);
  // Ket noi toi mang WiFi
  Serial.println();
  Serial.println();
  Serial.print("Ket noi toi mang ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Da ket noi WiFi");

  server.on("/", handle_OnConnect);
  server.on("/LEDON", handle_ledOn);
  server.on("/LEDOFF", handle_ledOff);
  server.onNotFound(handle_NotFound);
  // Khoi dong server
  server.begin();
  Serial.println("Khoi dong Server");
  Serial.println(WiFi.localIP());
}

void loop() {
  // Kiem tra khi co client ket noi
  server.handleClient();
}
void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

void callSensor(){
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2); 
  digitalWrite(trigPin, HIGH); //Sets the trigPin on HIGH state for 10 micro seconds
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH); //read the echoPin, returns the sound wave travel time in microseconds
  distance = duration * 0.034 / 2; //Tính khoảng cách
  //distance = duration / 29.1 / 2;

  Serial.print("Khoảng cách đo được ="); //Hiện khoảng cách ra Serial Monitor
  Serial.print(distance);
  Serial.println("cm");
}
void handle_OnConnect(){
  callSensor();
//  d = ultrasonic.read();
//  d = ultrasonic.distanceRead();
  
  server.send(200, "text/html", SendHTML(distance,"NaN"));
}
String SendHTML(float d, String state){
  String s = "\r\n\r\n";
  s += "<head>";
  s += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
  s += "<meta http-equiv=\"refresh\" content=\"1\" />";
  s += "<style>body{font-size: 24px;} .voffset {margin-top: 30px;}</style>";
  s += "</head>";
  
  s += "<div class=\"container\">";
  s += "<h2>Theo doi khoang cach</h2>";
  s += "<div class=\"row voffset\">";
  s += "<div class=\"col-md-3\">Khoang cach do duoc: "+String(d)+"cm</div>";
   s += "<h3>LED:" + state + "</h3>";
  s += "<a href=\"/LEDON\"\"><button>ON</button></a>";
  s += "<a href=\"/LEDOFF\"\"><button>OFF</button></a>";
  s += "</div>";
  return s;
}
void handle_ledOn() {
  digitalWrite(D0, HIGH);
  callSensor();
  server.send(200, "text/html", SendHTML(distance,"ON"));

}
void handle_ledOff() {
  digitalWrite(D0, LOW);
  callSensor();
  server.send(200, "text/html", SendHTML(distance, "OFF"));
}
