#include <IRremote.h>

int RECV_PIN = 2;

IRrecv irrecv(RECV_PIN);
decode_results results;

void setup()
{
  pinMode(12, OUTPUT); //led giả bộ phát hồng ngoại
  pinMode(13, OUTPUT); //led phát sáng khi module nhận được hồng ngoại
  Serial.begin(9600);
  irrecv.enableIRIn();
}

void loop() {
  digitalWrite(12, HIGH);
  if (digitalRead(2) > 0)
    digitalWrite(13, LOW);
  else
    digitalWrite(13, HIGH);

  if (irrecv.decode(&results)) {
    if (results.bits > 0) {
      Serial.println(results.value, HEX);
      irrecv.resume();
    }
  }
  delay(100);
}
