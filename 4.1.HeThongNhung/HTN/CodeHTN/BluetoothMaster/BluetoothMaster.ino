#include <SoftwareSerial.h>

SoftwareSerial BTSerial(10, 11); // RX | TX
bool previousState = true;
void setup()
{
  Serial.begin(9600);
  pinMode(2,INPUT);
  BTSerial.begin(9600);  
}

void loop()
{
    bool state = digitalRead(2);
    if (state!=previousState){
    if(state){
      BTSerial.write('b');
    }else{
      BTSerial.write('t');
    }
    previousState = state;
    }
}
