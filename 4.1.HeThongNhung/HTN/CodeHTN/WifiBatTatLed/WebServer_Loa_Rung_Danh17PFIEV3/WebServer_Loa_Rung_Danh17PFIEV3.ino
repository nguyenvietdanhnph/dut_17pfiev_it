// Them thu vien
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#define RUNG_PIN D0
#define LOA_PIN D2

// Thong so WiFi nha ban
const char* ssid = "pfiev";  // Enter SSID here
const char* password = "123456789";  //Enter Password here
bool state = false;
// Tao server
ESP8266WebServer server(80);

void setup() {
  // Mo Serial
  Serial.begin(115200);
  delay(10);
  pinMode(RUNG_PIN, INPUT);
  pinMode(LOA_PIN, OUTPUT);
  Serial.println();
  Serial.println();
  Serial.print("Ket noi toi mang ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Da ket noi WiFi");


  server.on("/", handle_OnConnect);
  server.on("/LOAON", handle_loaOn);
  server.on("/LOAOFF", handle_loaOff);
  server.onNotFound(handle_NotFound);

  // Khoi dong server
  server.begin();
  Serial.println("Khoi dong Server");
  Serial.println(WiFi.localIP());
}

void loop() {

  // Kiem tra khi co client ket noi
  server.handleClient();
}
void handle_OnConnect() {
  docRung();
  server.send(200, "text/html", SendHTML());
}
void handle_NotFound() {
  server.send(404, "text/plain", "Not found");
}
String SendHTML() {
  String s = "\r\n\r\n";
  s += "<head>";
  s += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
  s += "<meta http-equiv=\"refresh\" content=\"1\" />";
  s += "<style>body{font-size: 24px;} .voffset {margin-top: 30px;}</style>";
  s += "</head>";
  s += "<h3>RUNG:" + String(state) +"</h3>";
  s += "<a href=\"/LOAON\"\"><button>ON</button></a>";
  s += "<a href=\"/LOAOFF\"\"><button>OFF</button></a>";
  return s;
}

void docRung() {
  state = digitalRead(RUNG_PIN);
  Serial.println(state);
}

void handle_loaOn() {
  docRung();
  tone(LOA_PIN, 1000);
  server.send(200, "text/html", SendHTML());

}
void handle_loaOff() {
  docRung();
  noTone(LOA_PIN);
  server.send(200, "text/html", SendHTML());
}
