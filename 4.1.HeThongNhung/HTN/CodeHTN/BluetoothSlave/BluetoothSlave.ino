#include <SoftwareSerial.h>
#include <Servo.h>
#include <Stepper.h>
Servo myservo;
int pos;

const int stepsPerRevolution = 2048;  // change this to fit the number of steps per revolution

Stepper myStepper(stepsPerRevolution, 6, 7, 8, 9);

SoftwareSerial BTSerial(10,11); // RX | TX

void setup()
{
  pinMode(2,OUTPUT);
  Serial.begin(9600);
  myStepper.setSpeed(10);
  //myservo.attach(9); 
  BTSerial.begin(9600);  // HC-05 default speed in AT command more
}
void clearCommand(){
  while (BTSerial.available()) BTSerial.read();
}
void loop()
{
  if (BTSerial.available()){
    //Serial.write(BTSerial.read());
    char readchar = BTSerial.read();
    Serial.print(readchar);
    if(readchar=='t'){
      digitalWrite(2,HIGH);
      //myservo.write(90);
      myStepper.step(stepsPerRevolution);
     
    }
    if(readchar=='b'){
      digitalWrite(2,LOW);
     // myStepper.step(0);
     myStepper.step(0);
 
      //myservo.write(0);
    }
}}
