#define ledPin 13
#define vibPin 3
bool state=0;

void setup() {
  pinMode(ledPin,OUTPUT);
  pinMode(vibPin,INPUT);
  attachInterrupt(1,blink,FALLING);
}

void loop() {
  if (state){
    state = 0;
    digitalWrite(ledPin, HIGH);
    delay(500);  
  }
  else {
    digitalWrite(ledPin,LOW);
  }
}

void blink(){
  state = !state;
}
