﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using QLSV_MYSQL.BLL;

namespace QLSV_MYSQL
{
    public partial class Form1 : Form
    {
        public SV_BLL bll{ get; set; }
        public Form1()
        {
            InitializeComponent();
            string cnnString = @"Data Source=DESKTOP-FLB8J0F\SQLEXPRESS;Initial Catalog=QLSV;Integrated Security=True";
            bll = new SV_BLL(cnnString);
            SetCBB();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bll.GetALLSV();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SV s = new SV { MSSV = textBox1.Text, NameSV = textBox2.Text, Gender = radioButton1.Checked, NS = dateTimePicker1.Value, ID_Lop = Convert.ToInt32(((CBBItem)comboBox1.SelectedItem).Value) };
            bll.AddSV(s);
            dataGridView1.DataSource = bll.GetALLSV();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SV s = new SV { MSSV = textBox1.Text, NameSV = textBox2.Text, Gender = radioButton1.Checked, NS = dateTimePicker1.Value, ID_Lop = Convert.ToInt32(((CBBItem)comboBox1.SelectedItem).Value) };
            bll.UpdateSV(s);
            dataGridView1.DataSource = bll.GetALLSV();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SV s = new SV
            {
                MSSV = textBox1.Text,
            };
            bll.DeleteSV(s);
            dataGridView1.DataSource = bll.GetALLSV();      
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bll.SearchSV(textBox4.Text);
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewSelectedRowCollection r = dataGridView1.SelectedRows;
            if (r.Count == 1)
            {
                textBox1.Text = r[0].Cells["MSSV"].Value.ToString();
                textBox2.Text = r[0].Cells["NameSV"].Value.ToString();
                comboBox1.Text = r[0].Cells["ID_Lop"].Value.ToString();
                dateTimePicker1.Value = (DateTime)r[0].Cells["NS"].Value;
                radioButton1.Checked = (bool)r[0].Cells["Gender"].Value;
                radioButton2.Checked = !radioButton1.Checked;
            }
        }
        public void SetCBB()
        {
            foreach(CBBItem i in bll.GetCBBSV())
            {
                comboBox1.Items.Add(i);
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string re = ((CBBItem)comboBox1.SelectedItem).Value;
            MessageBox.Show(re);
        }
    }
}
