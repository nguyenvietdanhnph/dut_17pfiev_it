﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSV_MYSQL.BLL
{
    public class SV_BLL
    {
        public SV_DAL dal { get; set; }
        public SV_BLL(string s)
        {
            dal = new SV_DAL(s);
        }
        public List<SV> GetALLSV()
        {
            string query = "select * from SV";
            return dal.GetListSV(query);
        }
        public void AddSV(SV s)
        {
            dal.AddSV(s);
        }
        public void UpdateSV(SV s)
        {
            dal.UpdateSV(s);
        }
        public void DeleteSV(SV s)
        {
            dal.DeleteSV(s);
        }
        public List<SV> SearchSV(string s)
        { 
            string query = "SELECT * FROM SV WHERE NameSV LIKE '%" + s + "%'";
            return dal.GetListSV(query);
        }
        public List<CBBItem> GetCBBSV()
        {
            List<CBBItem> data = new List<CBBItem>();
            string query = "select * from LopSH";
            foreach (LOPSH i in dal.GetListLopSH(query))
            {
                data.Add(new CBBItem { Text = i.LopSH.ToString(), Value = i.ID_Lop.ToString() });
            }
            return data;
        }
    }
}
