﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QLSV_MYSQL
{
    public class SV_DAL
    {
        public DBHelper db { get; set; }
        public SV_DAL(string s)
        {
            db = new DBHelper(s);
        }
        public List<SV> GetListSV(string query)
        {
            List<SV> data = new List<SV>();
            foreach (DataRow i in db.GetRecords(query).Rows)
            {
                data.Add(GetSV(i));
            }
            return data;
        }
        public SV GetSV(DataRow d)
        {
            return (new SV
            {
                MSSV = d["MSSV"].ToString(),
                NameSV = d["NameSV"].ToString(),
                Gender = Boolean.Parse(d["Gender"].ToString()),
                NS = DateTime.Parse(d["NS"].ToString()),
                ID_Lop = Int32.Parse(d["ID_Lop"].ToString()),
            });
        }
        public List<LOPSH> GetListLopSH(string query)
        {
            List<LOPSH> data = new List<LOPSH>();
            foreach (DataRow i in db.GetRecords(query).Rows)
            {
                data.Add(GetLopSH(i));
            }
            return data;
        }
        public LOPSH GetLopSH(DataRow d)
        {
            return (new LOPSH
            {
                ID_Lop = Int32.Parse(d["ID_Lop"].ToString()),
                LopSH = d["LopSH"].ToString()
            });
        }
        public void AddSV(SV s)
        {
            string query = "INSERT INTO SV (MSSV, NameSV, Gender, NS, ID_Lop) VALUES ('" + s.MSSV + "', '" + s.NameSV + "', '" + s.Gender + "', '" + s.NS + "', '" + s.ID_Lop + "')";
            db.Execute(query);
        }
        public void UpdateSV(SV s)
        {
            string query = "UPDATE SV SET NameSV = '" + s.NameSV + "', Gender = '" + s.Gender+ "', NS = '" + s.NS + "', ID_Lop = '" + s.ID_Lop + "' WHERE MSSV = " + s.MSSV;
            db.Execute(query);
        }
        public void DeleteSV(SV s)
        {
            string query = "DELETE FROM SV WHERE MSSV = " + s.MSSV;
            db.Execute(query);
        }
        
    }
}
