#include <GL/freeglut.h>
#include <math.h>
//Ham thuc hien cac thao tac ve theo yeu cau cua chuong trinh
void display(void)
{
    GLfloat x;

    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(0, 0.2, 0.3);
    //glBegin(GL_LINES);
    // x-axis
   // glVertex3f(-5.0, 0, 0.0);
   // glVertex3f(5.0, 0, 0.0);
    // y-axis
   // glVertex3f(0, -5.0, 0.0);
    //glVertex3f(0, 5.0, 0.0);
   // glEnd();
    glBegin(GL_LINE_STRIP);
    // for (x = -5.0; x <= 5.0; x += 0.001)
    // {
    //     glVertex2f(x, sin(x));
    // }
    // glEnd();
    // glBegin(GL_LINE_STRIP);
    // for (x = -5.0; x <= 5.0; x += 0.001)
    // {
    //     glVertex2f(x, cos(x));
    // }
    for (x = -10.0; x <= 10.0; x+= 0.001) {
        glVertex2f(x + 2.0*sin(2.0*x), x + 2.0*cos(5.0*x));
    }
    glEnd();
    glFlush();
}

// Ham thuc hien cac khoi tao
void init(void)
{
    //Chon mau de xoa nen
    glClearColor(0.9, 1.0, 0.9, 0.0); /* mau #e6ffe6 */
    //Thiet lap cac thong so cho view
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-6.5, 6.5, -8, 8, -1.0, 1.0);
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    //Khoi tao che do ve single buffer va he mau RGB
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    //Khoi tao window kich thuoc 300 x 300
    glutInitWindowSize(500, 500);
    //Khoi tao window tai vi tri (100,100) tren screen
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Sin and cos"); //Ten cua window la 'Sin and cos'
    init();                          //Khoi tao mot so che do do hoa
    glutDisplayFunc(display);        //Thiet lap ham ve la ham display()
    glutMainLoop();                  //Bat dau chu trinh lap the hien ve
    return 0;
}
