#include <GL/freeglut.h>
//Ham thuc hien cac thao tac ve theo yeu cau cua chuong trinh
void display(void)
{
    //Xoa moi pixel
    glClear(GL_COLOR_BUFFER_BIT);

    // ve hinh chu nhat
    glColor3f(0.0, 0.5, 0.5);  //Thiet lap mau ve: #008080
    glBegin(GL_POLYGON);       //Bat dau ve da giac
    glVertex3f(0.2, 0.2, 0.0); //cac dinh cua da giac
    glVertex3f(0.8, 0.2, 0.0);
    glVertex3f(0.8, 0.6, 0.0);
    glVertex3f(0.2, 0.6, 0.0);
    glEnd(); //Ket thuc ve da giac
    //Ve mai nha
    glColor3f(1.0, 0.0, 0.0);  //Thiet lap mau ve: mau do
    glBegin(GL_POLYGON);       //Bat dau ve da giac
    glVertex3f(0.2, 0.6, 0.0); //cac dinh cua da giac
    glVertex3f(0.5, 0.8, 0.0);
    glVertex3f(0.8, 0.6, 0.0);
    glEnd();
    //Ve cua chinh
    glColor3f(0.6, 0.2, 0.2);  //Thiet lap mau ve: #993333
    glBegin(GL_POLYGON);       //Bat dau ve da giac
    glVertex3f(0.4, 0.2, 0.0); //cac dinh cua da giac
    glVertex3f(0.6, 0.2, 0.0);
    glVertex3f(0.6, 0.45, 0.0);
    glVertex3f(0.4, 0.45, 0.0);
    glEnd();
    //Ve cua so trai
    glBegin(GL_POLYGON);        //Bat dau ve da giac
    glVertex3f(0.2, 0.35, 0.0); //cac dinh cua da giac
    glVertex3f(0.3, 0.35, 0.0);
    glVertex3f(0.3, 0.45, 0.0);
    glVertex3f(0.2, 0.45, 0.0);
    glEnd();
    //Ve cua so phai
    glBegin(GL_POLYGON);        //Bat dau ve da giac
    glVertex3f(0.7, 0.35, 0.0); //cac dinh cua da giac
    glVertex3f(0.8, 0.35, 0.0);
    glVertex3f(0.8, 0.45, 0.0);
    glVertex3f(0.7, 0.45, 0.0);
    glEnd();
    //Ket thuc ve da giac
    glFlush();
}

// Ham thuc hien cac khoi tao
void init(void)
{
    //Chon mau de xoa nen
    glClearColor(0.9, 1.0, 0.9, 0.0); /* mau #e6ffe6 */
    //Thiet lap cac thong so cho view
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    //Khoi tao che do ve single buffer va he mau RGB
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    //Khoi tao window kich thuoc 300 x 300
    glutInitWindowSize(300, 300);
    //Khoi tao window tai vi tri (100,100) tren screen
    glutInitWindowPosition(100, 100);
    glutCreateWindow("house"); //Ten cua window la 'house'
    init();                    //Khoi tao mot so che do do hoa
    glutDisplayFunc(display);  //Thiet lap ham ve la ham display()
    glutMainLoop();            //Bat dau chu trinh lap the hien ve
    return 0;
}
