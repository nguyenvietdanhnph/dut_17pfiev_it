#include <GL/freeglut.h>
//Ham thuc hien cac thao tac ve theo yeu cau cua chuong trinh
void display(void)
{
    //Xoa moi pixel
    glClear(GL_COLOR_BUFFER_BIT);

    // ve hinh chu nhat co diem trai-tren va phai-duoi
    // (0.25, 0.25, 0.0) and (0.75, 0.75, 0.0)
    glColor3f(1.0, 1.0, 1.0);    //Thiet lap mau ve: mau trang
    glBegin(GL_POLYGON);         //Bat dau ve da giac
    glVertex3f(0.25, 0.25, 0.0); //cac dinh cua da giac
    glVertex3f(0.75, 0.25, 0.0);
    glVertex3f(0.75, 0.75, 0.0);
    glVertex3f(0.25, 0.75, 0.0);
    glEnd(); //Ket thuc ve da giac
    // thuc hien qua trinh day ra buffer
    glFlush();
}

// Ham thuc hien cac khoi tao
void init(void)
{
    //Chon mau de xoa nen
    glClearColor(0.0, 0.0, 0.0, 0.0); /* mau den */
    //Thiet lap cac thong so cho view
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    //Khoi tao che do ve single buffer va he mau RGB
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    //Khoi tao window kich thuoc 250 x 250
    glutInitWindowSize(250, 250);
    //Khoi tao window tai vi tri (100,100) tren screen
    glutInitWindowPosition(100, 100);
    glutCreateWindow("rectangle"); //Ten cua window la ‘rectangle’
    init();                        //Khoi tao mot so che do do hoa
    glutDisplayFunc(display);      //Thiet lap ham ve la ham display()
    glutMainLoop();                //Bat dau chu trinh lap the hien ve
    return 0;
}
