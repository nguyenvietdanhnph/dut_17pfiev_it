% chuong trinh lam tron 1 tin hieu de khu nhieu 
% dung bo loc lay trung binh cong 3 diem (3-points moving-averaging filter).
clf;                            % clear figures
L = 51;                         % do dai tin hieu
n = 0:L-1;                      % bien thoi gian roi rac
d = 0.5*randn(1,L);             % sinh tin hieu Gausian noise d[n]
s = 2*n.*(0.9.^n);              % sinh tin hieu goc s[n] = 2n(0.9)^n
x = s + d;                      % tin hieu co nhieu x[n]=s[n]+d[n]

figure(1)                       % tao figure 1
%hold on
subplot(3,2,1)                  % tao luoi 3x1 plots va ve plot o vi tri #1
plot(n,d,'r-',n,s,'k--',n,x,'b-.'); % ve do thi d[n],s[n],x[n]
xlabel('Chi so thoi gian n');
ylabel('Bien do');
legend('d[n]','s[n]','x[n]');
title('Noise vs. original vs. noisy signals');

% cach 1: dich thoi gian, lam tron tin hieu theo CT y[n] = 1/3(x[n-1]+x[n]+x[n+1])
x1 = [x(2:L), 0]        % x1 = x[n+1]
x2 = [x]                % x2 = x[n]
x3 = [0, x(1:L-1)]      % x3 = x[n-1]

subplot(3,2,2)
% ve do thi x[n-1],x[n],x[n+1]
plot(n,x1,'r-.',n,x2,'b-.',n,x3,'k-.');
xlabel('Chi so thoi gian n');
ylabel('Bien do');
legend('x[n+1]','x[n]','x[n-1]');
title('time-shifted signals of x[n]');

y1 = 1/3*(x1+x2+x3)     % lay trung binh voi M=1
subplot(3,2,3)
% ve do thi y[n] vs. s[n]
plot(n,y1(1:L),'r-',n,s(1:L),'b-');
xlabel('Chi so thoi gian n');
ylabel('Bien do');
legend('y1[n]','s[n]');
title('3-points smoothed vs. original signal');

% cach 2: dung ham tinh tong chap conv()
% ghep noi tiep he som 1 don vi va he lay TB cong nhan qua
h = 1/3 * ones(1,3);    % h[n] = [1/3, 1/3, 1/3]
y2 = conv(x1, h);       % y2[n] = x1[n] * h[n]
% ve do thi y[n] vs. s[n]
figure(3)
%hold on
plot(n,y1,'r-',n,y2(1:L),'b-');
xlabel('Chi so thoi gian n');
ylabel('Bien do');
legend('y1[n]','y2[n]');
title('cach1  vs. cach2');

% cach 3: dung ham cai dat bo loc filter()???




