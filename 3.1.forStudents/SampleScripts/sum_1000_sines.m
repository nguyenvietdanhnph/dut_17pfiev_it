% parameters
N = 1000; % number of sines w/ diff. freq.
L = 200; % half-length of signal 

w_step = 2*pi/N; % freq. step
n = -L:L; % discrete-time axis of signal

signal = zeros(1,length(n)); % an empty signal
for w = -pi:w_step:pi % (N+1) times
   % create a sine signal w/ curr. freq.
   signal = signal + cos(w * n);
end
signal = signal/(N+1);
clf
plot(n, signal);
grid;






