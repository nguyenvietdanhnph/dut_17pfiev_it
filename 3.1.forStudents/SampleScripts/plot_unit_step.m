% parameter settings (constants)
L1 = 20;
L2 = 20;

% processing code
n = -L1:L2;% time-base vector
x = [zeros(1,L1), ones(1,L2+1)];% amplitude vector
y = [(n>=0)]; % define y based on n
subplot(2,2,1)
stem(n,x,'fill');% plot x[n]
title('Unit step x[n]');
xlabel('Time (n)');
ylabel('Amplitude');

subplot(2,2,2)
stem(n,y);% plot y[n]
title('Unit step y[n]');
xlabel('Time (n)');
ylabel('Amplitude');

subplot(2,2,3)
stem(n,x,'fill');% plot x[n]
title('Unit step z[n]');
xlabel('Time (n)');
ylabel('Amplitude');

subplot(2,2,4)
stem(n,y);% plot y[n]
title('Unit step p[n]');
xlabel('Time (n)');
ylabel('Amplitude');