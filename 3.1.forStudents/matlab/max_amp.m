clear all;
close all;
clc;
[x,Fs]=wavread('C:\Users\MI\OneDrive\Desktop\forStudents-20190930T161819Z-001\HuongDanBaoCao\test_signals\lab_male.wav');

%%thong so'
threshold = 0.03;
min_silence_time = 0.2; %thoi` gian silence toi' thieu?
ts = 0.01; %thoi` gian lay' mau
%cai` dat. thong so'
min_silence = floor(min_silence_time/ts);
spf = ts*Fs; %so' mau~ tren 1 khung (samples per frame)
fn = ceil(length(x)/spf); %so' khung (frames number)

%%lay' mau~ tin' hieu. theo tung` khung
frames = buffer(x,spf);

%%tinh' toan' ham` bien do. cuc. dai. (m_amp)
m_amp = zeros(1,fn);
for i=1:fn
    m_amp(i) = max(abs(frames(:,i)));
end

%%loc. khoang? lang.
%loc. khoang? lang. ngoai` nguong~
isSilence = m_amp < threshold; 
%loc. khoang? lang. <200ms
count = 1;
frames_count(1) = 1;
for i=2:fn
    if isSilence(i) ~= isSilence(i-1)
        count = count + 1;
        frames_count(count) = 1;
    else
        frames_count(count) = frames_count(count) + 1;
    end
end
index = 1;
for i=1:count
    if frames_count(i) < min_silence
        isSilence(index:(index+frames_count(i))) = 0;
    end
    index = index + frames_count(i);
end 
frames(:,isSilence) = nan;
voice = reshape(frames,1,[]);

%%xuat' ket' qua? ra man` hinh`
%ve~ do` thi. tieng' noi'
figure;
plot(x,'color', [0.16 0.24 0.24]);
hold on; 
plot(voice, 'color', [0.58 0.72 0.72]);
xlim([0 length(x)])
%tao. truc. toa. do. moi'
ax1 = gca;
set(ax1,'XColor',[0.16 0.24 0.24]);
set(ax1,'Ycolor',[0.16 0.24 0.24]);
ax1_pos = get(ax1,'Position');
ax2 = axes('Position',ax1_pos,'XAxisLocation','top','YAxisLocation','right','Color','none');
set(ax2,'XColor','r');
set(ax2,'Ycolor','r');
%ve~ do` thi. m_amp
i=1:fn;
line(i,m_amp,'Parent',ax2,'Color','r');
xlim([0 fn]);
%ve~ truc. phan biet.
for i=2:fn
    if isSilence(i) ~= isSilence(i-1)
        line([i i], ylim, 'color', 'b');
    end
end