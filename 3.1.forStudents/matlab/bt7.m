clear;
clc;
[x,Fs]=wavread('file1_to');
Ex1=sum(abs(x) .^ 2);
ZCR1=sum(abs(diff(x>0))) / (length(x)-1);
figure;
plot(x);

[y,Fy]=wavread('file2_nho');
Ex2=sum(abs(y) .^2);
ZCR2=sum(abs(diff(y>0))) / (length(y)-1);
figure;
plot(y);