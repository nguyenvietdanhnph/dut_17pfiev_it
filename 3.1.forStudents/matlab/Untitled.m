clear all;
close all;
clc;
[x,Fs]=wavread('C:\Users\MI\OneDrive\Desktop\forStudents-20190930T161819Z-001\HuongDanBaoCao\test_signals\studio_female.wav');

threshold = 0.01;
ts = 0.01; %thoi` gian lay' mau
spf = ts*Fs; %so' mau~ tren 1 khung (samples per frame)
fn = ceil(length(x)/spf); %so' khung (frames number)
frames = buffer(x,spf); %chia doan. tin' hieu. x

Fe=zeros(1,fn);
for i=1:fn
    Fe(i) = sum(abs(frames(:,i)) .^ 2);
end
Fe = Fe / max(abs(Fe));

%%phan biet.
id = find(Fe > threshold); %tim` id nhung~ khung co' ZCR < 0.2 (day la` nhung~ khung tieng' noi')
isVoice = Fe > threshold;

figure;
plot(x,'color', 'r');
ax1 = gca;
set(ax1,'XColor','r');
set(ax1,'Ycolor','r');
ax1_pos = get(ax1,'Position');
ax2 = axes('Position',ax1_pos,'XAxisLocation','top','YAxisLocation','right','Color','none');
i=1:fn;
line(i,Fe,'Parent',ax2,'Color','k');
%hold on;

%line(i,Fzcr(5),'Parent',ax2,'Color','g');

for i=1:fn-1
    if isVoice(i) ~= isVoice(i+1)
        line([i i],ylim ,'color', 'b', 'Parent', ax2);
    end
    
end


