#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#define FILE_LENGTH 0X100
struct Sinhvien
{
    char* HoLot;
    char Ten[10];
    int day, month, year;
    int GioiTinh;
    char DiaChi[50];
    char MaSV[15];
};
void Nhap(struct Sinhvien sv[], int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("Nhap sinh vien thu %d \n", i + 1);
        fflush(stdin);
        printf("Ho lot: ");
        while ((getchar()) != '\n'); 
        size_t bufsize = 20;
        sv[i].HoLot = (char*)malloc(bufsize * sizeof(char));
        getline(&(sv[i].HoLot), &bufsize, stdin);
        printf("Ten: ");
        scanf("%s", sv[i].Ten);
        printf("Ngay/thang/nam sinh: ");
        scanf("%d %d %d", &sv[i].day, &sv[i].month, &sv[i].year);
        printf("Nam(1)/Nu(2): ");
        scanf("%d", &sv[i].GioiTinh);
        printf("Dia chi: ");
        scanf("%s", sv[i].DiaChi);
    }
}
void SapXep(struct Sinhvien sv[], int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (strcmp(sv[i].Ten, sv[j].Ten) > 0)
            {
                struct Sinhvien temp = sv[i];
                sv[i] = sv[j];
                sv[j] = temp;
            }
            else if (strcmp(sv[i].Ten, sv[j].Ten) == 0)
            {
                if (strcmp(sv[i].HoLot, sv[j].HoLot) > 0)
                {
                    struct Sinhvien temp = sv[i];
                    sv[i] = sv[j];
                    sv[j] = temp;
                }
            }
        }
    }
}
void TaoMaSV(struct Sinhvien sv[], int n)
{
    for (int i = 0; i < n; i++)
    {
        char khoa[2], stt[2];
        sprintf(khoa, "%d", sv[i].year + 18 - 2000);
        strcat(sv[i].MaSV, "122");
        strcat(sv[i].MaSV, khoa);
        strcat(sv[i].MaSV, "00");
        sprintf(stt, "%d", i + 1);
        strcat(sv[i].MaSV, stt);
    }
}
int main(int argc, char *const argv[])
{
    int n;
    int fd;
    struct Sinhvien sv[100];
    void *file_memory;
    /* Chuan bi mot file du lon de chua so nguyen unsigned */
    fd = open(argv[1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    lseek(fd, FILE_LENGTH + 1, SEEK_SET);
    write(fd, "", 1);
    lseek(fd, 0, SEEK_SET);
    /* Tao bo nho anh xa */
    file_memory = mmap(0, FILE_LENGTH, PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
    printf("\nNhap thong tin sinh vien \n");
    printf("Nhap so luong sinh vien:");
    scanf("%d", &n);
    Nhap(sv, n);
    SapXep(sv, n);
    TaoMaSV(sv, n);
    int j;
    j = sprintf(file_memory, "%d\n", n);
    for (int i = 0; i < n; i++)
    {
        j += sprintf(file_memory + j, "%s %s %d %d %d %d %s %s\n", sv[i].HoLot, sv[i].Ten, sv[i].day, sv[i].month, sv[i].year, sv[i].GioiTinh, sv[i].DiaChi, sv[i].MaSV);
    }
    /* Go bo bo nho */
    munmap(file_memory, FILE_LENGTH);
    return 0;
}