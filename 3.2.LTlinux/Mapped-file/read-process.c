#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#define FILE_LENGTH 0x100
struct Sinhvien
{
    char HoLot[20];
    char Ten[10];
    int day, month, year;
    int GioiTinh;
    char DiaChi[20];
    char MaSV[15];
};
int Sochuso(int n)
{
    int i = 0;
    while (n >= 10)
    {
        i++;
        n = n / 10;
    }
    return i + 1;
}
int main(int argc, char *const argv[])
{
    struct Sinhvien sv[100];
    int fd;
    int i, n, j = 1;
    void *file_memory;
    int integer;
    /* Mo file */
    fd = open(argv[1], O_RDWR, S_IRUSR | S_IWUSR);
    /* Tao bo nho anh xa */
    file_memory = mmap(0, FILE_LENGTH, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
    printf("%s %13s %13s %15s %12s %21s\n", "HoLot", "|Ten", "|Ngay sinh", "|Gioi Tinh", "|DiaChi", "|MaSV");
    /* Doc thong tin sinh vien va in ra chung */
    sscanf(file_memory, "%d", &n);
    for (int i = 0; i < n; i++)
    {
        sscanf(file_memory + j, "%s %s %d %d %d %d %s %s\n", sv[i].HoLot, sv[i].Ten, &sv[i].day, &sv[i].month,
               &sv[i].year, &sv[i].GioiTinh, sv[i].DiaChi, sv[i].MaSV);
        j += strlen(sv[i].HoLot) + strlen(sv[i].Ten) + Sochuso(sv[i].day) + Sochuso(sv[i].month) + Sochuso(sv[i].year) + Sochuso(sv[i].GioiTinh) + strlen(sv[i].DiaChi) + strlen(sv[i].MaSV) + 8;
        if (sv[i].GioiTinh == 1)
        {
            if (strlen(sv[i].HoLot) > 8)
                printf("%s \t%s \t%d/%d/%d \t%s \t\t%s \t\t%s\n", sv[i].HoLot, sv[i].Ten, sv[i].day, sv[i].month, sv[i].year, "Nam", sv[i].DiaChi, sv[i].MaSV);
            if (strlen(sv[i].HoLot) < 8)
                printf("%s \t\t%s \t%d/%d/%d \t%s \t\t%s \t\t%s\n", sv[i].HoLot, sv[i].Ten, sv[i].day, sv[i].month,
                       sv[i].year, "Nam", sv[i].DiaChi, sv[i].MaSV);
        }
        if (sv[i].GioiTinh == 2)
        {
            if (strlen(sv[i].HoLot) > 8)
                printf("%s \t%s \t%d/%d/%d \t%s \t\t%s \t\t%s\n", sv[i].HoLot, sv[i].Ten, sv[i].day, sv[i].month, sv[i].year,
                       "Nu", sv[i].DiaChi, sv[i].MaSV);
            if (strlen(sv[i].HoLot) < 8)
                printf("%s \t\t%s \t%d/%d/%d \t%s \t\t%s \t\t%s\n", sv[i].HoLot, sv[i].Ten, sv[i].day, sv[i].month,
                       sv[i].year, "Nu", sv[i].DiaChi, sv[i].MaSV);
        }
    }
    /* Go bo bo nho */
    munmap(file_memory, FILE_LENGTH);
    return 0;
}