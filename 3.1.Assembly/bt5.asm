.model small
.stack 50
.data
  msg1 db "Nhap 1 ki tu: $"
  msg_morning db 10, 13, "Good morning!$"
  msg_afternoon db 10, 13, "Good afternoon!$"
  msg_evening db 10, 13, "Good evening!$"                     
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    lea dx, msg1
    int 21h
    
    mov ah, 1
    int 21h
    mov bl, al
    
    cmp bl, 'S'
    je istrue1:
    cmp bl, 's'
    jne nextinstr1
   istrue1: 
    mov ah, 9
    lea dx, msg_morning
    int 21h
   nextinstr1:
    
    cmp bl, 'T'
    je istrue2
    cmp bl, 't'
    jne nextinstr2 
   istrue2:
    mov ah, 9
    lea dx, msg_afternoon
    int 21h
   nextinstr2:  
   
    cmp bl, 'C'
    je istrue3
    cmp bl, 'c'
    jne nextinstr3
   istrue3:   
    mov ah, 9
    lea dx, msg_evening
    int 21h
   nextinstr3:
          
    mov ah, 4Ch
    int 21h
          
  main endp
  end main   
  
  
  