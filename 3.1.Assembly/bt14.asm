.model small
.stack 50
.data
  msg1 db "Nhap so A: $"
  msg2 db 10, 13, "Nhap so B: $"   
  msg3 db 10, 13, "A + B = $"
  msg4 db 10, 13, "A - B = $"
  chuoiA db 50, ?, 50 dup('$') 
  chuoiB db 50, ?, 50 dup('$')
  soA dw ?
  soB dw ?  
  nc10 dw 10
          
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    lea dx, msg1
    int 21h
    mov ah, 0Ah
    lea dx, chuoiA
    int 21h
    
    mov ah, 9
    lea dx, msg2
    int 21h
    mov ah, 0Ah
    lea dx, chuoiB
    int 21h
    
    ;chuyen thanh so
    lea si, chuoiA
    lea di, soA 
    call stoi  
    
    lea si, chuoiB
    lea di, soB
    call stoi
    
    mov ah, 9
    lea dx, msg3
    int 21h
    
    mov ax, soA
    add ax, soB
    call printNumber 
    
    mov ah, 9
    lea dx, msg4
    int 21h
    
    mov ax, soA
    sub ax, soB
    call printNumber
    
   exit:       
    mov ah, 4Ch
    int 21h 
  main endp 
  
  ;chuyen tu chuoi sang so (16bit)
  ;dau vao si
  ;return di  
  stoi proc
    xor ax, ax     
    xor bx, bx
    mov cl, [si+1]
    xor ch, ch   
    add si, 2
   chuyen:
    mul nc10
    mov bl, [si]
    add ax, bx
    sub ax, '0' 
    inc si
    loop chuyen 
  
    mov [di], ax
    ret
  stoi endp
  
  ;in so tu ax (16bit)
  printNumber proc
    xor cx, cx 
    xor dx, dx
    vaostack:  
        div nc10
        push dx
        inc cx
        xor dx,dx
        cmp ax, 0
        jne vaostack
        
    hienthi:    
        pop dx
        add dx, '0'
        mov ah, 2
        int 21h    
        loop hienthi
    
    ketthuc:
    ret
  printNumber endp
  
  
  
  end main   
  
  
  