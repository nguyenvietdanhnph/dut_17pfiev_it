.model small
.stack 50
.data
  msg1 db "Nhap ten: $"
  msg2 db 10, 13, "Xin chao $"
  hoten db 50, ?, 50 dup('$')                                
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    lea dx, msg1
    int 21h
    
    mov ah, 0Ah
    lea dx, hoten
    int 21h
    
    mov ah, 9
    lea dx, msg2
    int 21h
    
    mov ah, 9
    mov dx, offset hoten + 2
    int 21h
          
    mov ah, 4Ch
    int 21h
          
  main endp
  end main   
  
  
  