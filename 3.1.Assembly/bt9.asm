.model small
.stack 50
.data
  msg1 db "Nhap chuoi so 1: $"
  msg2 db 10, 13, "Nhap chuoi so 2: $"   
  msg3 db 10, 13, "Tong: $"
  chuoi1 db 50, ?, 50 dup('$') 
  chuoi2 db 50, ?, 50 dup('$')
  so1 dw ?
  so2 dw ?  
  nc10 dw 10
             
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    lea dx, msg1
    int 21h
    mov ah, 0Ah
    lea dx, chuoi1
    int 21h
    
    mov ah, 9
    lea dx, msg2
    int 21h
    mov ah, 0Ah
    lea dx, chuoi2
    int 21h
    
    ;chuyen thanh so
    lea si, chuoi1
    lea di, so1 
    call stoi  
    
    lea si, chuoi2
    lea di, so2
    call stoi
    
    mov ah, 9
    lea dx, msg3
    int 21h
    
    mov ax, so1
    add ax, so2
    call printNumber
    
   exit:       
    mov ah, 4Ch
    int 21h 
  main endp 
  
  ;chuyen tu chuoi sang so (16bit)
  ;dau vao si
  ;return di  
  stoi proc
    xor ax, ax     
    xor bx, bx
    mov cl, [si+1]
    xor ch, ch   
    add si, 2
   chuyen:
    mul nc10
    mov bl, [si]
    add ax, bx
    sub ax, '0' 
    inc si
    loop chuyen 
  
    mov [di], ax
    ret
  stoi endp
  
  ;in so tu ax (16bit)
  printNumber proc
    xor cx, cx 
    xor dx, dx
    vaostack:  
        div nc10
        push dx
        inc cx
        xor dx,dx
        cmp ax, 0
        jne vaostack
        
    hienthi:    
        pop dx
        add dx, '0'
        mov ah, 2
        int 21h    
        loop hienthi
    
    ketthuc:
    ret
  printNumber endp
  
  
  
  end main   
  
  
  