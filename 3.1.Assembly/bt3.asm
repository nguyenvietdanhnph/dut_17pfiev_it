.model small
.stack 50
.data
  msg1 db "Hay go 1 phim: $"  
  msg2 db 10, 13, "Ky tu ke truoc: $"  
  msg3 db 10, 13, "Ky tu ke sau: $"                               
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    lea dx, msg1
    int 21h
    
    mov ah, 1
    int 21h
    mov bl, al
    
    mov ah, 9
    lea dx, msg2
    int 21h
              
    dec bl          
    mov ah, 2
    mov dl, bl
    int 21h
    
    mov ah, 9
    lea dx, msg3
    int 21h
    
    add bl, 2
    mov ah, 2
    mov dl, bl
    int 21h
          
    mov ah, 4Ch
    int 21h
          
  main endp
  end main   
  
  
  