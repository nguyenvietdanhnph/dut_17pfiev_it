.model small
.stack 50
.data
    msg1 db "Nhap so: $" 
    msg2 db 10, 13, "Tong cac chu so: $"
    chuoi db 50, ?, 50 dup('$') 
    dem dw ?       
    nc10 dw 10
.code   
    main proc 
        mov ax, @data
        mov ds, ax
                         
        lea dx, msg1
        mov ah, 9
        int 21h
        
        lea dx, chuoi
        mov ah, 0Ah
        int 21h
        
        lea dx, msg2
        mov ah, 9
        int 21h
        
        mov cl, chuoi[1]
        xor ch, ch
        mov bx, 2
        mov ax, 0
       tong: 
        mov dl, chuoi[bx]
        sub dl, '0'
        xor dh, dh
        add ax, dx
        inc bx
        loop tong
        
        call printNumber
        
        mov ah, 4Ch
        int 21h
    main endp
    
;in so tu ax (16bit)
  printNumber proc
    xor cx, cx 
    xor dx, dx
    vaostack:  
        div nc10
        push dx
        inc cx
        xor dx,dx
        cmp ax, 0
        jne vaostack
        
    hienthi:    
        pop dx
        add dx, '0'
        mov ah, 2
        int 21h    
        loop hienthi
    
    ketthuc:
    ret
  printNumber endp
    
end main