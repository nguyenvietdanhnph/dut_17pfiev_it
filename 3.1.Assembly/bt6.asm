.model small
.stack 50
.data
  msg1 db "Nhap 1 ki tu thuong: $"
  nl db 10, 13, '$'                    
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    lea dx, msg1
    int 21h
    
    mov ah, 1
    int 21h
    mov cl, al
    
    mov ah, 9
    lea dx, nl
    int 21h
   
   Hienthi: 
    cmp cl, 'z'
    ja exit
    mov ah, 2
    mov dl, cl
    int 21h 
    mov ah, 2
    mov dl, ' '
    int 21h
    inc cl
    jmp Hienthi
    
   exit:       
    mov ah, 4Ch
    int 21h
          
  main endp
  end main   
  
  
  