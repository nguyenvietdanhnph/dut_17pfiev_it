.model small
.stack 50
.data 
  msgNhap1 db 'Nhap day 16bit A: $'
  msgNhap2 db 10, 13, 'Nhap day 16bit B: $'   
  msg_Tong db 10, 13, 'A + B = $'       
  msg_Hieu db 10, 13, 'A - B = $'   
  msg_And db 10, 13, 'A and B = $'
  msg_Or db 10, 13, 'A or B = $'
  mang1 db 16 dup(?)
  mang2 db 16 dup(?) 
  so1 dw 0
  so2 dw 0
.code
  main proc   
    mov ax, @data
    mov ds, ax      
    
  ;Nhap
    lea dx, msgNhap1
    mov ah, 9
    int 21h
    mov cx, 16
    lea bx, mang1
  Nhap1:
    mov ah, 1
    int 21h
    sub al, 30h
    mov [bx], al   
    shl so1, 1 
    mov ah, 0
    add so1, ax
    inc bx
    loop Nhap1
    
    lea dx, msgNhap2
    mov ah, 9
    int 21h
    mov cx, 16
    lea bx, mang2
  Nhap2:
    mov ah, 1
    int 21h
    sub al, 30h
    mov [bx], al  
    shl so2, 1
    mov ah,0
    add so2, ax
    inc bx
    loop Nhap2
  
    lea dx, msg_Tong
    mov ah, 9      
    int 21h
    mov bx, so1
    add bx, so2
    call inNhiPhan   
                    
    lea dx, msg_Hieu
    mov ah, 9
    int 21h          
    mov bx, so1
    sub bx, so2
    call inNhiPhan
    
    lea dx, msg_And
    mov ah, 9
    int 21h
    mov bx, so1
    and bx, so2 
    call inNhiPhan
    
    lea dx, msg_Or
    mov ah, 9
    int 21h
    mov bx, so1
    or bx, so2
    call inNhiPhan
    
    mov ah, 4ch
    int 21h
  main endp
  
  ;In ra so' nhi. phan tu` bx (16bit) 
  inNhiPhan proc 
        mov cx, 16
      Print:
        cmp bx, 0xF000
        jl In1  
      In0:
        mov dl, '0'
        mov ah, 2
        int 21h
        shl bx, 1   
        jmp Continue
      In1:
        mov dl, '1'
        mov ah, 2
        int 21h   
        shl bx, 1
      Continue:  
        loop Print   
        
        ret
  inNhiphan endp  
  
  end main