.model small
.stack 50
.data
   filename db "myfile.txt", 0  
   kytu db ?  
   dulieu10 db 11 dup('$')
   msg db "Nhap chuoi ket thuc bang #: $"    
   msg_kytu db 10, 13, "Doc tung ky tu: $"
   msg_block db 10, 13, "Doc tung block 10 ky tu: $"
   handle dw ?
.code
    main proc 
      mov ax, @data
      mov ds, ax
         
      mov cx, 0              ;tao file
      lea dx, filename
      mov ah, 3ch
      int 21h  
                             
      mov al, 1              ;mo file de viet
      lea dx, filename
      mov ah, 3dh
      int 21h
      mov handle, ax
              
      lea dx, msg
      mov ah, 9
      int 21h
     
     nhap:         
      mov ah, 1
      int 21h 
      cmp al, '#'
      je thoatNhap
      mov kytu, al
      lea dx, kytu
      mov bx, handle
      mov cx, 1
      mov ah, 40h
      int 21h
      jmp nhap
     thoatNhap:
      
      mov bx, handle           ;dong file
      mov ah, 3eh
      int 21h 
      
      mov al, 0                ;mo file de doc
      lea dx, filename
      mov ah, 3dh
      int 21h
      mov handle, ax    
      
      lea dx, msg_kytu
      mov ah, 9
      int 21h
     
     xuatKyTu:                ;xuat tung ky tu
      mov bx, handle           
      mov cx, 1
      lea dx, kytu
      mov ah, 3fh
      int 21h
      cmp ax, 0
      je thoatXuatKyTu  
      mov dl, kytu
      mov ah, 2
      int 21h
      jmp xuatKyTu
     thoatXuatKyTu:
      
      mov bx, handle           ;dong file
      mov ah, 3eh
      int 21h                  
      
;-------------------------------------------------------;      
      
      mov al, 0                ;mo file de doc
      lea dx, filename
      mov ah, 3dh
      int 21h
      mov handle, ax  
      
      lea dx, msg_block
      mov ah, 9
      int 21h
      
     xuatBlock10:              ;xuat block 10 ky tu
      mov bx, handle
      mov cx, 10
      lea dx, dulieu10
      mov ah, 3fh
      int 21h
      cmp ax, 0
      je thoatXuatBlock10 
      mov bx, ax 
      mov dulieu10[bx], '$' 
      lea dx, dulieu10
      mov ah, 9
      int 21h
      jmp xuatBlock10
     thoatXuatBlock10:
      
      mov bx, handle           ;dong file
      mov ah, 3eh
      int 21h  
      
      mov ah, 4ch
      int 21h
        
    main endp
    end main