.model small
.stack 50
.data
    msg1 db "Nhap chuoi: $" 
    msg2 db 10, 13, "Chuoi nguoc: $"
    chuoi db 50, ?, 50 dup('$')
.code   
    main proc 
        mov ax, @data
        mov ds, ax
                         
        lea dx, msg1
        mov ah, 9
        int 21h
        
        lea dx, chuoi
        mov ah, 0Ah
        int 21h
        
        lea dx, msg2
        mov ah, 9
        int 21h
        
        mov cl, chuoi[1]
        xor ch, ch
        mov bx, cx
       inNguoc: 
        mov dl, b.chuoi[bx+1]
        mov ah, 2
        int 21h
        dec bx
        loop inNguoc
        
        mov ah, 4Ch
        int 21h
    main endp
    
end main