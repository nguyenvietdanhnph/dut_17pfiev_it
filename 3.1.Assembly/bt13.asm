.model small
.stack 50
.data
    msgNhapA db "Nhap so 16bit dang Hex A: $"  
    msgNhapB db 10, 13, "Nhap so 16bit dang Hex B: $" 
    msg_Tong db 10, 13, 'A + B = $'       
    msg_Hieu db 10, 13, 'A - B = $'   
    msg_And db 10, 13, 'A and B = $'
    msg_Or db 10, 13, 'A or B = $'
    chuoiA db 50, ?, 50 dup('$')  
    chuoiB db 50, ?, 50 dup('$')
    soA dw ?    
    soB dw ?
    nc16 dw 16
.code 
  main proc
    mov ax, @data
    mov ds, ax
    
    lea dx, msgNhapA
    mov ah, 9
    int 21h  
    lea dx, chuoiA
    mov ah, 0Ah   
    int 21h       
               
    lea dx, msgNhapB
    mov ah, 9
    int 21h           
    lea dx, chuoiB
    mov ah, 0Ah
    int 21h
    
    lea si, chuoiA
    lea di, soA
    call strHexToInt  
                 
    lea si, chuoiB
    lea di, soB
    call strHexToInt 
    
    lea dx, msg_Tong
    mov ah, 9      
    int 21h
    mov bx, soA
    add bx, soB
    call inNhiPhan   
                    
    lea dx, msg_Hieu
    mov ah, 9
    int 21h          
    mov bx, soA
    sub bx, soB
    call inNhiPhan
    
    lea dx, msg_And
    mov ah, 9
    int 21h
    mov bx, soA
    and bx, soB 
    call inNhiPhan
    
    lea dx, msg_Or
    mov ah, 9
    int 21h
    mov bx, soA
    or bx, soB
    call inNhiPhan            
    
    mov ah, 4ch
    int 21h
   
   main endp  
    
  ;chuyen tu chuoi sang so (16bit)
  ;dau vao si
  ;return di  
  strHexToInt proc
    xor ax, ax     
    xor bx, bx
    mov cl, [si+1]
    xor ch, ch   
    add si, 2
   chuyen:
    mul nc16
    mov bl, [si] 
    cmp bl, '9'
    jg ABCDEF
    sub bl, '0' 
    jmp outABCDEF:
   ABCDEF:
    or bl, 00100000b  
    sub bl, 87
   outABCDEF:
    add ax, bx
    inc si
    loop chuyen 
  
    mov [di], ax
    ret
  strHexToInt endp  
  
  ;In ra so' nhi. phan tu` bx (16bit) 
  inNhiPhan proc 
        mov cx, 16
      Print:
        cmp bx, 0xF000
        jl In1  
      In0:
        mov dl, '0'
        mov ah, 2
        int 21h
        shl bx, 1   
        jmp Continue
      In1:
        mov dl, '1'
        mov ah, 2
        int 21h   
        shl bx, 1
      Continue:  
        loop Print   
        
        ret
  inNhiphan endp  
   
   end main