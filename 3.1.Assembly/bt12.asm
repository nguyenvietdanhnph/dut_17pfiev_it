.model small
.stack 50
.data
    msg db "Nhap 1 ki tu: $"
    msg_hex db 10, 13, "Hex: $"
    msg_dec db 10, 13, "Dec: $"
    msg_bin db 10, 13, "Bin: $" 
    chia10 db 10      
    chia2 db 2  
    chia16 db 16
.code
    main proc
        mov ax, @data
        mov ds, ax 
        
        lea dx, msg
        mov ah, 9
        int 21h
        mov ah, 1
        int 21h   
        mov bl, al
        
        lea dx, msg_hex
        mov ah, 9
        int 21h
        mov al, bl
        call inHex
        
        lea dx, msg_dec
        mov ah, 9
        int 21h
        mov al, bl 
        call inDec
        
        lea dx, msg_bin
        mov ah, 9
        int 21h
        mov al, bl
        call inBin
        
        mov ah, 4ch
        int 21h
        
    main endp      
    
;In dang. Hex tu` al  
  inHex proc
    xor ah, ah
    xor cx, cx
   vaoStackHex: 
    cmp ax, 0
    je printHex
    div chia16
    mov dl, ah
    xor dh, dh
    push dx
    inc cx
    xor ah, ah
    jmp vaoStackHex 
    
   printHex:
    pop dx   
    cmp dl, 9
    jg inABCDF
    add dl, '0'
    jmp outABCDF
   inABCDF:
    add dl, 'A'
   outABCDF: 
    mov ah, 2
    int 21h
    loop printHex 
    
    ret
  inHex endp

;In dang Dec tu` al  
  inDec proc
    xor ah, ah
    xor cx, cx
   vaoStackDec: 
    cmp ax, 0
    je printDec
    div chia10
    mov dl, ah
    xor dh, dh
    push dx
    inc cx
    xor ah, ah
    jmp vaoStackDec 
    
   printDec:
    pop dx  
    add dl, '0'
    mov ah, 2
    int 21h
    loop printDec 
    
    ret
  inDec endp    
 
;In dang Bin tu` al  
  inBin proc
    xor ah, ah
    xor cx, cx
   vaoStackBin: 
    cmp ax, 0
    je printDec
    div chia2
    mov dl, ah
    xor dh, dh
    push dx
    inc cx
    xor ah, ah
    jmp vaoStackBin 
    
   printBin:
    pop dx  
    add dl, '0'
    mov ah, 2
    int 21h
    loop printBin 
    
    ret
  inBin endp
     
    
    end main