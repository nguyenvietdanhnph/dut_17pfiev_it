.model small
.stack 50
.data
  msg1 db "Nhap chuoi: $"
  msg2 db 10, 13, "Chuoi: $"   
  msg3 db 10, 13, "So ki tu trong chuoi la: $"
  xau db 50 dup(?) 
  chia10 db 10               
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    mov dx, offset msg1
    int 21h
    
    lea bx, xau   ;nhap vao xau
    xor cx, cx
   nhap: 
    mov ah, 1
    int 21h
    cmp al, 13
    je nextinstr
    xor ah, ah
    mov [bx], al
    inc bx
    inc cx
    jmp nhap
   nextinstr: 
    
    mov [bx], '$'
    
    mov ah, 9
    lea dx, msg2
    int 21h
    
    mov ah, 9
    lea dx, xau
    int 21h
    
    mov ah, 9
    lea dx, msg3
    int 21h
    
    mov ax, cx
    call printNumber
    
   exit:       
    mov ah, 4Ch
    int 21h
          
  main endp 
  
  printNumber proc; in so tu ax ;co thay doi cac thanh ghi ax,cx,dx
    xor cx, cx
    vaostack:  
        div chia10
        mov dl, ah
        xor dh, dh
        push dx
        inc cx
        xor ah, ah
        cmp al, 0
        jne vaostack
        
    hienthi:    
        pop dx
        add dx, '0'
        mov ah, 2
        int 21h    
        loop hienthi
    
    ketthuc:
    ret
  printNumber endp
  
  end main   
  
  
  