.model small
.stack 50
.data
    msg1 db "Nhap n: $"  
    msghoanhao db 10, 13, "La so hoan hao$"
    msg0hoanhao db 10, 13, "Khong phai so hoan hao$"
    chuoiN db 50, ?, 50 dup('$')
    n dw ?
    tonguoc dw ?  
    nc10 dw 10  
.code   
    main proc
        mov ax, @data
        mov ds, ax
        
        lea dx, msg1
        mov ah, 9
        int 21h
        
        lea dx, chuoiN
        mov ah, 0Ah
        int 21h
        
        lea si, chuoiN
        lea di, n
        call stoi
        
        mov bx, 0
        mov cx, n
        shr cx, 1
       hoanHao: 
        mov ax, n
        xor dx, dx
        div cx
        cmp dx, 0
        jne checkFalse
        add bx, cx
       checkFalse: 
        loop hoanHao
        
        cmp bx, n
        je laSoHoanHao
        lea dx, msg0hoanhao
        mov ah, 9
        int 21h
        jmp thoat
       laSoHoanHao: 
        lea dx, msghoanhao
        mov ah, 9
        int 21h
       thoat:
        
        mov ah, 4Ch
        int 21h 
    main endp    
    
;chuyen tu chuoi sang so (16bit)
  ;dau vao si
  ;return di  
  stoi proc
    xor ax, ax     
    xor bx, bx
    mov cl, [si+1]
    xor ch, ch   
    add si, 2
   chuyen:
    mul nc10
    mov bl, [si]
    add ax, bx
    sub ax, '0' 
    inc si
    loop chuyen 
  
    mov [di], ax
    ret
  stoi endp
  
end main