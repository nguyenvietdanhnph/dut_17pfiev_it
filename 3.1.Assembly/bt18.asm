printNumber macro k
    local vaostack, hienthi           
    mov ax, k
    ;in so tu ax (16bit)
    xor cx, cx 
    xor dx, dx
    vaostack:  
        div nc10
        push dx
        inc cx
        xor dx,dx
        cmp ax, 0
        jne vaostack
        
    hienthi:    
        pop dx
        add dx, '0'
        mov ah, 2
        int 21h    
        loop hienthi
endm

inputNumber macro k
    local chuyen
    lea dx, k_temp
    mov ah, 0Ah   
    int 21h   
    
    lea si, k_temp
    lea di, k
    ;chuyen tu chuoi sang so (16bit)
  ;dau vao si
  ;return di  
    xor ax, ax     
    xor bx, bx
    mov cl, [si+1]
    xor ch, ch   
    add si, 2
   chuyen:
    mul nc10
    mov bl, [si]
    add ax, bx
    sub ax, '0' 
    inc si
    loop chuyen 
  
    mov [di], ax 
endm

.model small
.stack 50
.data   
    msg0 db "Nhap so luong: $"
    msg1 db 10, 13, "Nhap so thu $"
    msg2 db ": $"
    msg3 db 10, 13, "Trung binh cong: $"
    n dw ?
    i dw ?
    so dw ?    
    tong dw ?
    nc10 dw 10
    k_temp db 50, ?, 50 dup('$')
.code
    main proc
        mov ax, @data
        mov ds, ax
        mov es, ax
        
        lea dx, msg0
        mov ah, 9
        int 21h 

        inputNumber n
        
        mov i, 1
       nhap:
        lea dx, msg1
        mov ah, 9
        int 21h
        
        printNumber i
        
        lea dx, msg2
        mov ah, 9
        int 21h
        
        inputNumber so
        mov ax, so
        add tong, ax  
        inc i
        mov ax, i
        cmp ax, n
        jle nhap
        
        lea dx, msg3
        mov ah, 9
        int 21h
        
        mov dx, 0
        mov ax, tong
        div n
        
        printNumber ax 
        
        mov ah, 4Ch
        int 21h    
    main endp

end main