.model small
.stack 50
.data
    msg1 db "Nhap n: $"         
    msg2 db 10, 13, "n! = $"
    chuoiN db 50, ?, 50 dup('$')
    n dw ?    
    nc10 dw 10 
    fact dw ?
.code   
    main proc 
        mov ax, @data
        mov ds, ax
        
        lea dx, msg1
        mov ah, 9
        int 21h
        
        lea dx, chuoiN
        mov ah, 0Ah
        int 21h
        
        ;chuyen thanh so
        lea si, chuoiN
        lea di, n
        call stoi
        
        mov cx, n
        mov ax, 1
       giaithua: 
        mul cx
        loop giaithua
        mov fact, ax 
                      
        lea dx, msg2
        mov ah, 9
        int 21h
        
        mov ax, fact              
        call printNumber
        
        mov ah, 4Ch
        int 21h
    main endp

  ;chuyen tu chuoi sang so (16bit)
  ;dau vao si
  ;return di  
  stoi proc
    xor ax, ax     
    xor bx, bx
    mov cl, [si+1]
    xor ch, ch   
    add si, 2
   chuyen:
    mul nc10
    mov bl, [si]
    add ax, bx
    sub ax, '0' 
    inc si
    loop chuyen 
  
    mov [di], ax
    ret
  stoi endp
    
;in so tu ax (16bit)
  printNumber proc
    xor cx, cx 
    xor dx, dx
    vaostack:  
        div nc10
        push dx
        inc cx
        xor dx,dx
        cmp ax, 0
        jne vaostack
        
    hienthi:    
        pop dx
        add dx, '0'
        mov ah, 2
        int 21h    
        loop hienthi
    
    ketthuc:
    ret
  printNumber endp
end main