.model small
.stack 50
.data
  msg1 db "Nhap chuoi: $"
  msg2 db 10, 13, "Thuong: $"   
  msg3 db 10, 13, "Hoa: $"
  xau db 50, ?, 50 dup('$') 
               
.code
  
  main proc  
    mov ax, @data
    mov ds, ax  
    
    mov ah, 9
    lea dx, msg1
    int 21h
    
    mov ah, 0Ah
    lea dx, xau
    int 21h
    
    mov bx, offset xau + 1
    mov cl, [bx]
    mov bx, offset xau + 2
   thuong:
    or [bx], 00100000b  ;chuyen thanh chu thuong
    inc bx
    loop thuong
    
    mov ah, 9
    lea dx, msg2
    int 21h
    
    mov ah, 9
    mov dx, offset xau + 2
    int 21h
    
    mov bx, offset xau + 1
    mov cl, [bx]
    mov bx, offset xau + 2
   hoa:
    cmp [bx], ' '
    je continue
    and [bx], 11011111b ;chuyen thanh hoa
   continue:
    inc bx
    loop hoa
    
    mov ah, 9
    lea dx, msg3
    int 21h
    
    mov ah, 9
    mov dx, offset xau +2
    int 21h 
    
   exit:       
    mov ah, 4Ch
    int 21h
          
  main endp 
  
  end main   
  
  
  