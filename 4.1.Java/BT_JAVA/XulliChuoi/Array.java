//3) Nhap 1 mang 1 chieu gom n so thuoc kieu int
// a) Tong cac so nguyen le co trong mang
// b) SX mang da cho theo thu tu tang dan
// c) Chen phan tu k sao cho van giu thu tu tang dan
// d) Xoa di so le dau tien cua mang
 
package LuongVaoRa;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExampleArray {
	public int nhapSo() throws IOException
	{
		InputStreamReader luongvao = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(luongvao);
		int s = Integer.parseInt(br.readLine()) ;
		return s;
	}
	public void showArr(int[] arr) {
		// TODO Auto-generated method stub
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + "   ");		
		}
	}
	public void sumOdd(int[] arr) {
		// TODO Auto-generated method stub
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] % 2 != 0) {
				sum += arr[i];
			}
		}
		System.out.println("Tong cac so nguyen le: " + sum);
	}
	public void sortArr(int[] arr) {
		int temp = arr[0];
        for (int i = 0 ; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
	}
	public int [] insertElement(int [] arr, int k) {
        int arrIndex = arr.length - 1;
        int tempIndex = arr.length;
        int [] tempArr = new int [tempIndex + 1];
        boolean inserted = false;
         
        for (int i = tempIndex; i >= 0; i--) {
            if (arrIndex > -1 && arr[arrIndex] > k) {
                tempArr[i] = arr[arrIndex--];
            } else {
                if (!inserted) {
                    tempArr[i] = k;
                    inserted = true;
                } else {
                    tempArr[i] = arr[arrIndex--];
                }
            }
        }
        return tempArr;
    }
	public static void main(String[] args) {
		ExampleArray a = new ExampleArray();
		System.out.println("Nhap so phan tu");
		
		try {
			int n = a.nhapSo();
			int[] arr = new int[n];
			for (int i = 0; i < n; i++) {
				System.out.print("Nhap arr[" + i +']');
				arr[i] = a.nhapSo();
			}
			//a.showArr(arr);
			//a.sumOdd(arr);
			a.sortArr(arr);
			a.showArr(arr);
			System.out.print("Nhap so k: ");
			int k = a.nhapSo();
			arr = a.insertElement(arr,k);
			a.showArr(arr);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
		}
		
	}
}
