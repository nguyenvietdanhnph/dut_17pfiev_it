package crud;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Add extends JFrame implements ActionListener{
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private static String DB_URL = "jdbc:mysql://localhost:3306/course?serverTimezone=UTC";
    private static String USER_NAME = "root";
    private static String PASSWORD = "";
	JButton btnClear, btnAdd;
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btnAdd) {
			try {
				Connection conn = getConnection(DB_URL, USER_NAME, PASSWORD);		
				String queryString = "insert into coursetable values('"+textField.getText()+"','"+
				textField_1.getText()+"','"+Integer.parseInt(textField_2.getText()) +"')";
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(queryString);
				System.out.print("Add successfully");
				conn.close();
			} catch (Exception ex) {
				// TODO: handle exception
				ex.printStackTrace();
			}
		}
	}
	public Add(String title) {
		  super(title);
		  GUI();
		}
	public void GUI() {
		setBounds(100, 100, 606, 240);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Code");
		lblNewLabel.setBounds(22, 26, 91, 22);
		contentPane.add(lblNewLabel);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(22, 83, 91, 22);
		contentPane.add(lblName);
		
		JLabel lblCredit = new JLabel("Credit");
		lblCredit.setBounds(22, 132, 91, 22);
		contentPane.add(lblCredit);
		
		textField = new JTextField();
		textField.setBounds(123, 27, 96, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(123, 84, 357, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(124, 133, 114, 20);
		contentPane.add(textField_2);
		
		btnAdd = new JButton("Add");
		btnAdd.setBounds(111, 169, 89, 23);
		contentPane.add(btnAdd);
		
		btnClear = new JButton("Clear");
		btnClear.setBounds(316, 169, 89, 23);
		contentPane.add(btnClear);
		btnAdd.addActionListener(this);
		show();
	}
	public static Connection getConnection(String dbURL, String userName, String password) {
	    Connection conn = null;
	    try {
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        conn = DriverManager.getConnection(dbURL, userName, password);
	        System.out.println("connect successfully!");
	    } catch (Exception ex) {
	        System.out.println("connect failure!");
	        ex.printStackTrace();
	    }
	    return conn;
	}
}