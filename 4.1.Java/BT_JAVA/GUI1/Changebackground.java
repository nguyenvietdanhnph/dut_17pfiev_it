import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Changebackground extends Frame implements ActionListener{
	Button red, blue, green, exit;
	Panel pn1, pn2;
	public void GUI() {
		red = new Button("red");
		blue = new Button("blue");
		green = new Button("green");
		exit = new Button("Exit");
		setLayout(new FlowLayout());	
		add(red);
		add(blue);
		add(green);
		red.addActionListener(this);
		blue.addActionListener(this);
		green.addActionListener(this);
		setSize(300, 300);
		show();
	}
	public Changebackground(String title) {
		super(title);
		GUI();
	}
public static void main(String args[]) {
new Changebackground("Change background");

}
@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	if(e.getSource() == red) {
		setBackground(Color.red);
	}
	if(e.getSource() == blue) {
		setBackground(Color.blue);
	}
	if(e.getSource() == green) {
		setBackground(Color.green);
	}
	if(e.getSource()==exit) {
		this.disable();
	}
}
}
