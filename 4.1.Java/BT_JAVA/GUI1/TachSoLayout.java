import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TachSoLayout extends Frame implements ActionListener {
	Panel contentPane;
	TextField textField;
	Button btnNewButton;
	TextArea textArea;
	Choice choice;
	Button btnNewButton_1;
	public void GUI() {
		// TODO Auto-generated method stub
		setBounds(100, 100, 581, 299);
		contentPane = new Panel();
		contentPane.setLayout(null);
		add(contentPane);
		Label lblNewLabel = new Label("Nhap so tu nhien n:");
		lblNewLabel.setBounds(37, 13, 127, 16);
		contentPane.add(lblNewLabel);
		
		textField = new TextField();
		textField.setBounds(176, 10, 169, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		textArea = new TextArea();
		btnNewButton = new Button("Xoa");
		btnNewButton.addActionListener(this);
		btnNewButton.setBounds(413, 9, 97, 25);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("Tach theo chieu: ");
		lblNewLabel_1.setBounds(37, 62, 114, 16);
		contentPane.add(lblNewLabel_1);
		
		btnNewButton_1 = new Button("Tach so");
		btnNewButton_1.setBounds(413, 58, 97, 25);
		btnNewButton_1.addActionListener(this);
		contentPane.add(btnNewButton_1);
		
		choice = new Choice();
		choice.setBounds(176, 62, 169, 22);
		choice.addItem("Tu trai sang phai");
		choice.addItem("Tu phai sang trai");
		contentPane.add(choice);
		
		JLabel lblNewLabel_2 = new JLabel("Ket qua tach duoc: ");
		lblNewLabel_2.setBounds(37, 127, 97, 16);
		contentPane.add(lblNewLabel_2);
		
		
		textArea.setBounds(176, 124, 300, 92);
		contentPane.add(textArea);
		setVisible(true);
	}
	public TachSoLayout(String str) {
		// TODO Auto-generated constructor stub
		super(str);
		GUI();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnNewButton) {
			textArea.getText();
			textArea.setText("");
			textField.getText();
			textField.setText("");
		}
		if (e.getSource() == btnNewButton_1) {
			String str = textField.getText();
			String textA = "";
			if (choice.getSelectedIndex() == 0) {
				int i = 1;
				textA += (char)((int)str.charAt(0));
				do {
					textA += ',';
					textA += (char)((int)str.charAt(i));
					i++;				
				} while (i < str.length());
			}
			else 
			{
				int i = str.length() - 1;
				textA += (char)((int)str.charAt(i));
				do {
					i--;
					textA += ',';
					textA += (char)((int)str.charAt(i));
				} while (i > 0);
			}
			textArea.setText(textA);
		}
	}
	public static void main(String[] args) {
		new TachSoLayout("Tach so");

	}
}