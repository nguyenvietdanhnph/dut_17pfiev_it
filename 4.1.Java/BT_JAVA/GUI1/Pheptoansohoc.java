import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


import java.awt.*;
public class Pheptoansohoc extends JFrame implements ActionListener  {
	JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	JButton btn_cong, btnTru, btnNhan, btnChia, btnExit, btn_reset;
	public Pheptoansohoc(String title) {
		super(title);
		GUI();
	}


public void GUI() {
	setBounds(100, 100, 491, 380);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(null);
	add(contentPane);
	
	JLabel lblNewLabel = new JLabel("Minh hoa phep toan co ban");
	lblNewLabel.setBounds(20, 21, 251, 27);
	contentPane.add(lblNewLabel);
	
	JLabel lblNhapA = new JLabel("Nhap a");
	lblNhapA.setBounds(20, 69, 91, 27);
	contentPane.add(lblNhapA);
	
	JLabel lblNhapB = new JLabel("Nhap b");
	lblNhapB.setBounds(20, 124, 91, 27);
	contentPane.add(lblNhapB);
	
	JLabel lblKetQua = new JLabel("Ket qua");
	lblKetQua.setBounds(20, 188, 91, 27);
	contentPane.add(lblKetQua);
	
	textField = new JTextField();
	textField.setBounds(158, 72, 245, 20);
	contentPane.add(textField);
	textField.setColumns(10);
	
	textField_1 = new JTextField();
	textField_1.setColumns(10);
	textField_1.setBounds(158, 127, 245, 20);
	contentPane.add(textField_1);
	
	textField_2 = new JTextField();
	textField_2.setColumns(10);
	textField_2.setBounds(158, 191, 245, 20);
	contentPane.add(textField_2);
	
	btn_cong = new JButton("Cong");
	btn_cong.setBounds(20, 246, 91, 23);
	contentPane.add(btn_cong);
	
	btnTru = new JButton("Tru");
	btnTru.setBounds(141, 246, 89, 23);
	contentPane.add(btnTru);
	
	btnNhan = new JButton("Nhan");
	btnNhan.setBounds(265, 246, 89, 23);
	contentPane.add(btnNhan);
	
	btnChia= new JButton("Chia");
	btnChia.setBounds(378, 246, 89, 23);
	contentPane.add(btnChia);
	
	btnExit = new JButton("exit");
	btnExit.setBounds(111, 293, 89, 23);
	contentPane.add(btnExit);
	
	btn_reset = new JButton("reset");
	btn_reset.setBounds(314, 293, 89, 23);
	btn_cong.addActionListener(this);
	btnTru.addActionListener(this);
	btnNhan.addActionListener(this);
	btnChia.addActionListener(this);
	btn_reset.addActionListener(this);
	btnExit.addActionListener(this);
	contentPane.add(btn_reset);
	
	show();
	
}
@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	float a = Float.parseFloat(textField.getText());
	float b = Float.parseFloat(textField_1.getText());
	if(e.getSource() == btn_cong) {
		textField_2.setText(Float.toString(a+b));
	}
	if(e.getSource() == btn_cong) {
		textField_2.setText(Float.toString(a+b));
	}
	if(e.getSource() == btnTru) {
		textField_2.setText(Float.toString(a-b));
	}
	if(e.getSource() == btnNhan) {
		textField_2.setText(Float.toString(a*b));
	}
	if(e.getSource() == btnChia) {
		textField_2.setText(Float.toString(a/b));
	}
	if(e.getSource() == btn_reset) {
		textField.setText("");
		textField_1.setText("");
	}
	if(e.getSource() == btnExit) {
		this.dispose();
	}
	
}

public static void main(String args[]) {
	  new Pheptoansohoc("Phep toan");
}
}
