import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class ListCourse extends JFrame implements ActionListener {
	private JPanel contentPane;
	private static String DB_URL = "jdbc:mysql://localhost:3306/course";
    private static String USER_NAME = "root";
    private static String PASSWORD = "";
	public ListCourse(String title) {
		super(title);
		GUI();
	}
	public void GUI() {
		setBounds(100, 100, 606, 240);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("List of all course");
		lblNewLabel.setBounds(234, 11, 138, 40);
		contentPane.add(lblNewLabel);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(64, 51, 518, 141);
		contentPane.add(textArea);
		try {
			Connection conn = getConnection(DB_URL, USER_NAME, PASSWORD);	
			 Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from coursetable");       
            ResultSetMetaData rsmd = rs.getMetaData();
            int numCol = rsmd.getColumnCount(); //Get number of column
            String text="";
            text += "Code     |  Name     |  Credit\n";
            while (rs.next()) {
            	for(int i=1; i<=numCol;i++) {
            		text +=rs.getString(i) + "    " ;
            		if(i!=numCol) {
            			text+=" |  ";
            		}
            	}
            	text +="\n";
            }
            
            // close connection
            conn.close();
            textArea.setText(text);
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		show();
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	public static Connection getConnection(String dbURL, String userName, String password) {
	    Connection conn = null;
	    try {
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        conn = DriverManager.getConnection(dbURL, userName, password);
	        System.out.println("connect successfully!");
	    } catch (Exception ex) {
	        System.out.println("connect failure!");
	        ex.printStackTrace();
	    }
	    return conn;
	}
}
