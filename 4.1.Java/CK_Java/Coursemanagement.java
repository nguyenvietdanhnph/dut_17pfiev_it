import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
public class Coursemanagement extends JFrame implements ActionListener {
	JPanel contentPane;
	JButton btnAdd, btnDis, btnAdd_search,btn_exit;
public Coursemanagement(String title) {
  super(title);
  GUI();
}

public void GUI() {
	setBounds(100, 100, 586, 380);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(null);
	setContentPane(contentPane);
	
	JLabel lblNewLabel = new JLabel("Courses Management");
	lblNewLabel.setBounds(225, 22, 131, 33);
	contentPane.add(lblNewLabel);
	
	btnAdd = new JButton("Add a new Course");
	btnAdd.setBounds(135, 80, 300, 40);
	contentPane.add(btnAdd);
	
	btnDis = new JButton("Display all Course");
	btnDis.setBounds(135, 151, 300, 40);
	contentPane.add(btnDis);
	
	btnAdd_search = new JButton("Search Course by Course Code");
	btnAdd_search.setBounds(135, 226, 300, 40);
	contentPane.add(btnAdd_search);
	
	btn_exit = new JButton("Exit Application");
	btn_exit.setBounds(135, 292, 300, 40);
	btnAdd.addActionListener(this);
	btnDis.addActionListener(this);
	btnAdd_search.addActionListener(this);
	btn_exit.addActionListener(this);
	contentPane.add(btn_exit);
	show();
}

@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	if(e.getSource() == btnAdd) {
		new AddCourse("Add New Course");
	}
	if(e.getSource() == btnDis) {
		new ListCourse("List of all Course");
	}
	if(e.getSource() == btnAdd_search) {
		new SearchCode("Search Course by Course Code");
	}
	if(e.getSource() == btn_exit) {
		this.dispose();
	}
	
}
public static void main(String args[]) {
	  new Coursemanagement("Courses Management");
}
}
